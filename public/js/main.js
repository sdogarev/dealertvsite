$(function() {
    /**
     * header_balance_delaer
     */
    let header_balance_delaer = $('#header_balance_delaer');
    if(0 < header_balance_delaer.length) {
        let url_ajax = header_balance_delaer.data('url-ajax-balance');
        $.ajax({
            url: url_ajax,
            success: function(data){
                header_balance_delaer.html('<i class="icon-user"></i> Баланс: ' + data.data.toFixed(2) +'€');
            }
        });
    }

    /**
     * Получение всех абонементов.
     * @type {jQuery|HTMLElement}
     */
    let table_sub_index = $('#table_sub_index');
    if (0 < table_sub_index.length) {
        const url_ajax = $('#url_ajax').val();

        let d = new Date();

        let yy = d.getFullYear();
        if (yy < 10) yy = '0' + yy;

        let mm = (d.getMonth() + 1);
        if (mm < 10) mm = '0' + mm;

        let dd = d.getDate() ;
        if (dd < 10) dd = '0' + dd;

        let m = d.getMinutes();
        if (m < 10) m = '0' + m;

        let h = d.getHours();
        if (h < 10) h = '0' + h;

        let s = d.getSeconds();
        if (s < 10) s = '0' + s;

        let current_time = yy +"-"+ mm + "-" + dd +" "+ h +":"+ m + ":" + s ;

        let t = table_sub_index.DataTable({
            "ajax": url_ajax,
            "async": true,
            "dataSrc": "data",
            "language": {
                "emptyTable": "Данные отсутствуют в таблице",
                "lengthMenu": "Показать записи _MENU_ ",
                "info": "Показано от _START_ до _END_ всего _TOTAL_ ",
                "search": "Поиск:",
                "processing": "Загрузка",
                "loadingRecords": "<i class='fas fa-spinner'></i>",
                "paginate": {
                    "previous": "Назад",
                    "next": "Вперёд",
                }
            },
            "columns": [
                {"data": "login"},
                {"data": "client_id"},
                {"data": "expire"},
                {"data": "id"},
            ],

            "order": [[1, 'asc']],
            "createdRow": function (row, data, dataIndex) {
                // console.log(data);
            },
            'columnDefs': [
                {
                    'targets': 0,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        let link = $('#url_client_sub_edit').val();
                        $(td).html("<a href='" + link.replace('XXX', rowData.id) + "'>" + rowData.login + "</a>");
                    }
                },
                {
                    'targets': 1,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        let link = $('#url_clients_edit').val();
                        $(td).html("<a href='" + link.replace('XXX', rowData.client_id) + "'>" + rowData.client + "</a>");
                    }
                },
                {
                    'targets': 2,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        if(current_time > rowData.expire) {
                            $(td).html("<span class='label label-md label-danger'>Завершён</span>");
                        } else {
                            $(td).html("<span class='label label-md label-success'>Активен</span>");
                        }
                    }
                },
                {
                    'targets': 3,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        let link = $('#url_sub_edit').val();
                        $(td).html("<a class='btn btn-primary btn-modal' href='"+ link.replace('XXX', rowData.id) +"'>Продлить</a>");
                    }
                },
            ]
        });
    }

    /**
     * Получение всех абонементов, активных, не активных абонементов клиента.
     * @type {jQuery|HTMLElement}
     */
    let table_clients_sub_index = $('#table_clients_sub_index');
    if(0 < table_clients_sub_index.length) {

        let client_id = $('#client_id').val();
        let d = new Date();

        let yy = d.getFullYear();
        if (yy < 10) yy = '0' + yy;

        let mm = (d.getMonth() + 1);
        if (mm < 10) mm = '0' + mm;

        let dd = d.getDate() ;
        if (dd < 10) dd = '0' + dd;

        let m = d.getMinutes();
        if (m < 10) m = '0' + m;

        let h = d.getHours();
        if (h < 10) h = '0' + h;

        let s = d.getSeconds();
        if (s < 10) s = '0' + s;

        let current_time = yy +"-"+ mm + "-" + dd +" "+ h +":"+ m + ":" + s ;

        switch ($('#uri_check').val()) {
            case "expired":
                let link1 = $('#ajax_abonements_expired').val();
                var url = link1.replace('XXX',client_id);
                break;
            case "subscriptions":
                let link2 = $('#ajax_abonements').val();
                var url = link2.replace('XXX',client_id);
                break;
            case "active":
                let link3 = $('#ajax_abonements_active').val();
                var url = link3.replace('XXX',client_id);
                break;
        }
        // console.log(url);
        let t = table_clients_sub_index.DataTable({
            "ajax" : url,
            "async": true,
            "dataSrc": "data",
            "language": {
                "emptyTable": "Данные отсутствуют в таблице",
                "lengthMenu": "Показать записи _MENU_ ",
                "info": "Показано от _START_ до _END_ всего _TOTAL_ ",
                "search":  "Поиск:",
                "processing" : "Загрузка",
                "loadingRecords" : "<i class='fas fa-spinner'></i>",
                "paginate": {
                    "previous": "Назад",
                    "next": "Вперёд",
                }
            },
            "columns" : [
                {"data" : "login"},
                {"data" : "pass"},
                {"data" : "expire"},
                {"data" : "login"},
                {"data" : "stream_server"},
                {"data" : "timezone"},
                {"data" : "timezone"},
                {"data" : "notifyexpiry"},
            ],

            "order": [[ 1, 'asc' ]],
            "createdRow": function( row, data, dataIndex ) {
                // console.log(data);
            },
            'columnDefs': [
                {
                    'targets': 0,
                    'createdCell':  function (td, cellData, rowData, row, col) {
                        console.log(rowData)
                        let link = $('#url_clients_sub_edit').val();
                        $(td).html("<a href='"+ link.replace('XXX', rowData.id) +"'>"+ rowData.login +"</a>");
                    }
                },
                {
                    'targets': 6,
                    'createdCell':  function (td, cellData, rowData, row, col) {
                        if(current_time > rowData.expire) {
                            $(td).html("<span class='label label-md label-danger'>Завершён</span>");
                        } else {
                            $(td).html("<span class='label label-md label-success'>Активен</span>");
                        }
                    }
                },
                {
                    'targets': 7,
                    'createdCell':  function (td, cellData, rowData, row, col) {
                        let link = $('#url_sub_edit').val();
                        $(td).html("<a class='btn btn-primary btn-modal' href='"+ link.replace('XXX', rowData.id) +"'>Продлить</a>");
                    }
                },
            ]
        });
    }

    /**
     * Получение всех клиентов.
     * @type {jQuery|HTMLElement}
     */
    let table_clients = $('#table_clients_index');
    if(0 < table_clients.length) {
        const url_ajax = $('#url_ajax').val();
        let t = table_clients.DataTable({
            "ajax": url_ajax,
            "async": true,
            "dataSrc": "data",
            "language": {
                "emptyTable": "Данные отсутствуют в таблице",
                "lengthMenu": "Показать записи _MENU_ ",
                "info": "Показано от _START_ до _END_ всего _TOTAL_ ",
                "search": "Поиск:",
                "processing": "Загрузка",
                "loadingRecords": "<i class='fas fa-spinner'></i>",
                "paginate": {
                    "previous": "Назад",
                    "next": "Вперёд",
                }
            },
            "columns": [
                {"data": "name"},
                {"data": "email"},
                {"data": "phone"},
                {"data": "subscription"},
                {"data": "subscritpion_active"},
                {"data": "subscritpion_expend"},
                {"data": "reseller"},
            ],

            "order": [[1, 'asc']],
            "createdRow": function (row, data, dataIndex) {
                // console.log(data);
            },
            'columnDefs': [
                {
                    'targets': 0,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        let link = $('#url_clients_edit').val();
                        $(td).html("<a href='" + link.replace('XXX', rowData.id) + "'>" + rowData.name + "</a>");
                    }
                },
                {
                    'targets': 3,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        let link = $('#url_clients_sub').val();
                        $(td).html("<a href='" + link.replace('XXX', rowData.id) + "'>" + rowData.subscription + "</a>");
                    }
                },
                {
                    'targets': 4,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        let link = $('#url_clients_sub_active').val();
                        $(td).html("<a href='" + link.replace('XXX', rowData.id) + "'>" + rowData.subscritpion_active + "</a>");
                    }
                },
                {
                    'targets': 5,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        let link = $('#url_clients_sub_expire').val();
                        $(td).html("<a href='" + link.replace('XXX', rowData.id) + "'>" + rowData.subscritpion_expend + "</a>");
                    }
                },
                {
                    'targets': 6,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).html("<a class='btn btn-primary btn-modal' href='subscriptions/create?client_id=" + rowData.id + "'>Создать</a>");
                    }
                },
            ]
        });
    }

    /**
     * Получение всех тестовых абонементов.
     * @type {jQuery|HTMLElement}
     */
    let table_demo_index = $('#table_demo_index');
    if(0 < table_demo_index.length) {
        let d = new Date();

        let yy = d.getFullYear();
        if (yy < 10) yy = '0' + yy;

        let mm = (d.getMonth() + 1);
        if (mm < 10) mm = '0' + mm;

        let dd = d.getDate() ;
        if (dd < 10) dd = '0' + dd;

        let m = d.getMinutes();
        if (m < 10) m = '0' + m;

        let h = d.getHours();
        if (h < 10) h = '0' + h;

        let s = d.getSeconds();
        if (s < 10) s = '0' + s;

        let current_time = yy +"-"+ mm + "-" + dd +" "+ h +":"+ m + ":" + s ;
        const url_ajax = $('#url_ajax').val();
        let t = table_demo_index.DataTable({
            "ajax" : url_ajax,
            "async": true,
            "dataSrc": "data",
            "language": {
                "emptyTable": "Данные отсутствуют в таблице",
                "lengthMenu": "Показать записи _MENU_ ",
                "info": "Показано от _START_ до _END_ всего _TOTAL_ ",
                "search":  "Поиск:",
                "processing" : "Загрузка",
                "loadingRecords" : "<i class='fas fa-spinner'></i>",
                "paginate": {
                    "previous": "Назад",
                    "next": "Вперёд",
                }
            },
            "columns" : [
                {"data" : "login"},
                {"data" : "pass"},
                {"data" : "protect_code"},
                {"data" : "created"},
                {"data" : "expire"},
                {"data" : "id"},
            ],

            "order": [[ 1, 'asc' ]],
            'columnDefs': [
                {
                    'targets': 0,
                    'createdCell':  function (td, cellData, rowData, row, col) {
                        let link = $('#url_demo_edit').val();
                        $(td).html("<a href='"+ link.replace('XXX', rowData.id) +"'>"+ rowData.login +"</a>");
                    }
                },
                {
                    'targets': 5,
                    'createdCell':  function (td, cellData, rowData, row, col) {
                        if(current_time > rowData.expire) {
                            $(td).html("<span class='label label-md label-danger'>Завершён</span>");
                        } else {
                            $(td).html("<span class='label label-md label-success'>Активен</span>");
                        }
                    }
                },
            ]
        });
    }
});
