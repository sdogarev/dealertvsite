<?php
return array(
    'DEMO_DAYS' => 7,
    'SETTINGS_STREAM_SERVER' => ['Europe North', 'Europe Central',
                                 'USA North', 'USA South',
                                 'Israel', 'Asia South'],
    'DEFAULT_USER_SETTINGS' => json_encode([
        'services.vod'                 => '0',
        'services.archive'             => '1',
        'settings.timeshift.value'     => '0',
        'settings.timezone.value'      => '+2',
        'settings.http_caching.value'  => '4000',
        'settings.stream_server.value' => 'Europe North',
        'settings.bitrate.value'       => '8000',
        'settings.number_of_sessions'  => 3,
    ]),
);
