<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::group([
//    'domain' => env('APP_DOMAIN'),
    'middleware' => ['web', 'auth']
], function () {
//    Route::get('/', function () {
//        return view('welcome');
//    })->name('welcome.index');
//    Route::get('/subscription/create', function () {
//        return view('users.create');
//    })->name('welcome.index');

    Route::get('/email', function () {
        $s = \App\User::find(11327)->first();
        Mail::to('dikodax468@mail35.net')
            ->send(New \App\Mail\SubscriptionBuyMail($s));
//        return new \App\Mail\SubscriptionBuyMail();
    });

    Route::get('/', 'IndexController@index')->name('index');

    /* Clients */
    //TODO client - s create, edit, update, store
    Route::get('clients', 'ClientsController@index')->name('clients.index');
    Route::get('clients/create', 'ClientsController@create')->name('client.create');
    Route::get('clients/{id}/edit', 'ClientsController@edit')->name('client.edit');
    Route::post('clients/{id}/update', 'ClientsController@update')->name('client.update');
    Route::post('clients/store', 'ClientsController@store')->name('client.store');
    Route::get('clients/{id}/subscriptions', 'ClientsController@subscriptions')->name('client.subscriptions.index');
    Route::get('clients/{id}/subscriptions/active', 'ClientsController@activeSubscriptions')->name('client.subscriptions.active.index');
    Route::get('clients/{id}/subscriptions/expired', 'ClientsController@expiredSubscriptions')->name('client.subscriptions.expired.index');
    //TODO =
//    Route::get('clients/{id}/subscriptions/edit', 'ClientsController@editSubscriptions')->name('clients.subscriptions.edit');
//    Route::post('clients/subscriptions/update', 'ClientsController@updateSubscriptions')->name('clients.subscriptions.update');

    /* Subscriptions */
    Route::get('subscriptions', 'UsersController@index')->name('subscriptions.index');
    Route::get('subscriptions/create', 'UsersController@create')->name('subscription.create');
    Route::post('subscriptions/store', 'UsersController@store')->name('subscription.store');
    Route::get('subscriptions/{id}/edit', 'UsersController@edit')->name('subscription.edit');
    Route::get('subscriptions/{id}/extend', 'UsersController@extend')->name('subscription.extend');
    Route::post('subscriptions/{id}/renew', 'UsersController@renew')->name('subscription.renew');
    Route::post('subscriptions/{id}/update', 'UsersController@update')->name('subscription.update');

    /* TestUsers */
    Route::get('demos', 'TestUsersController@index')->name('demos.index');
    Route::get('demos/create', 'TestUsersController@create')->name('demo.create');
    Route::get('demos/{id}/edit', 'TestUsersController@edit')->name('demo.edit');
    Route::post('demos/{id}/update', 'TestUsersController@update')->name('demo.update');
    Route::post('demos/store', 'TestUsersController@store')->name('demo.store');

    /* AJAX */
    Route::get('clients/ajax/referals', 'ClientsController@get_index_referals_ajax')->name('clients.ajax.index');
    Route::get('clients/ajax/abonements/{id}', 'ClientsController@get_index_abonements_ajax')->name('clients.abonements.ajax.index');
    Route::get('clients/ajax/abonements/expired/{id}', 'ClientsController@get_index_abonements_expired_ajax')->name('clients.abonements.expired.ajax.index');
    Route::get('clients/ajax/abonements/active/{id}', 'ClientsController@get_index_abonements_active_ajax')->name('clients.abonements.active.ajax.index');
    Route::get('demo/ajax/index', 'TestUsersController@get_index_demo_ajax')->name('clients.demo.ajax.index');
    Route::get('subscriptions/abonements/ajax/index', 'UsersController@get_index_subscription_ajax')->name('subscriptions.abonements.ajax.index');
    Route::get('dealer/ajax/balance', 'DealerController@dealerAjaxBalance')->name('dealer.ajax.balance');
});
