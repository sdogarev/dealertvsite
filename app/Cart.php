<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Cart extends Model
{
    /**
     * @var string
     */
    protected $table = 'orders';
    public $timestamps = false;

    protected $fillable = [
        'created', 'updated', 'currency', 'status', 'client_id', 'reseller_id', 'referral_id'
    ];

    public function mySubscriptions()
    {
        return self::hasOne('App\User', 'client_id');
    }

    public function discountkey()
    {
        return $this->hasMany('App\Discountkey', 'id', 'discount_id');
    }

    public function cart()
    {
        return $this->hasOne('App\Cart', 'client_id', 'id');
    }

    public function ordered_products()
    {
        return $this->hasMany('App\OrderedProduct', 'order_id', 'id');
    }
}
