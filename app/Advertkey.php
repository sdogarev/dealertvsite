<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Advertkey extends Model
{
    protected $table = 'advert_keys';
    public $timestamps = false;

}
