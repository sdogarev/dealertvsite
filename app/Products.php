<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Products extends Model
{
    /**
     * @var string
     */
    protected $table = 'products';
    public $timestamps = false;

    public function kit()
    {
        return $this->hasOne('App\User', 'kit_id');
    }
}
