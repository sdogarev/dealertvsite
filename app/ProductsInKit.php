<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class ProductsInKit extends Model
{
    /**
     * @var string
     */
    protected $table = 'products_in_kit';
    public $timestamps = false;



    public function product()
    {
        return $this->hasOne('App\User', 'client_id');
    }
}
