<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Settings extends Model
{
    /**
     * @var string
     */
    protected $table = 'settings';
    public $timestamps = false;

    protected $fillable = [
       'user_id', 'var_name', "var_value"
    ];
}
