<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Servers extends Model
{
    /**
     * @var string
     */
    protected $table = 'servers';
    public $timestamps = false;
}
