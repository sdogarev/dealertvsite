<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class TestUsers extends Model
{
    /**
     * @var string
     */
    protected $table = 'testusers';
    public $timestamps = false;

    protected $fillable = [
        'email', 'user_id', 'id_social', 'created', 'location', 'reseller_id'
    ];

    public function abonements()
    {
        return $this->hasMany('App\User', 'login', 'user_id');
    }
}
