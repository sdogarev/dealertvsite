<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Balance extends Model
{
    protected $table = 'balances';
    public $timestamps = false;

    protected $fillable = [
        'type', 'client_id', 'amount', 'currency', 'initiator', 'order_id', 'total'
    ];
}
