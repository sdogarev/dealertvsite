<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Dealer;
use App\Locations;
use App\Products;
use App\Settings;
use App\Streamingserver;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class ClientsController extends Controller
{
    /**
     * Получение всех клиентов с абонементами
     *
     * @return View
     */
    public function index()
    {
        $dealer = auth()->user();
        $referals = $dealer->referrals()->get()->map(function ($item){
            return collect($item)->merge([
                'subscription'        => $item->abonements()->count(),
                'subscritpion_active' => $item->abonements()
                    ->where('expire', '>', Carbon::now()->toDateTimeString())
                    ->count(),
                'subscritpion_expend' => $item->abonements()
                    ->where('expire', '<', Carbon::now()->toDateTimeString())
                    ->count()
            ]);
        });

        return view('clients.index', [
            'referals'     => $referals,
            'nav_item_css' => 1
        ]);
    }

    /**
     * Форма редактирование данных клиента
     *
     * @param  int  $id
     * @return View
     */
    public function edit($id)
    {
        $client = collect(Clients::find($id))->merge([
            'country_name' => Locations::find($client = Clients::find($id)['country'])['name']
        ]);
        return view('clients.edit', [
            'client'      => $client,
            'countries'   => Locations::where('un_member', 'yes')->get(),
            'nav_item_css'=> 1
        ]);
    }

    /**
     * Обновление данных клиента
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $request->validate([
            'name'    => 'required|min:2|max:50',
            'email'   => 'required|max:255|string|email|unique:App\Clients,email,'.$id.',id',
            'phone'   => 'required|regex:/[0-9]/|not_regex:/[a-z]/|min:9|numeric',
            'person'  => 'required|string',
            'country' => 'required|exists:App\Locations,id',
            'city'    => 'nullable|string|min:2|max:50',
            'address' => 'nullable|string|min:2|max:100',
            'zip'     => 'nullable|min:2|max:50',
        ]);

        DB::transaction(function () use ($id, $request) {
          $client = Clients::find($id);
          $client->update([
              'name'    => $request->name,
              'email'   => $request->email,
              'phone'   => $request->phone,
              'person'  => $request->person,
              'country' => $request->country,
              'city'    => $request->city,
              'address' => $request->address,
              'zip'     => $request->zip,
          ]);
        });

        return redirect()->route('clients.index', [
            'status'  => 'success',
            'message' => 'Клиент успешно изменён.'
        ]);
    }

    /**
     * Форма создания нового абонемента
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('clients.create', [
            'countries'   => Locations::where('un_member', 'yes')->get(),
            'nav_item_css'=> 1
        ]);
    }

    /**
     * Создание нового абонемента.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'reseller_username'     => 'required|exists:App\Clients,username',
            'phone_valid'           => 'required|numeric|max:1',
            'name'                  => 'required|min:2|max:255|string',
            'email'                 => 'required|min:2|max:255|string|email|unique:App\Clients,email',
            'password'              => 'required|min:8|max:255|confirmed',
            'password_confirmation' => 'required|min:8|max:255',
            'phone'                 => 'required|regex:/[0-9]/|not_regex:/[a-z]/|min:9',
            'country'               => 'required|exists:App\Locations,id',
            'city'                  => 'nullable|string|min:2|max:50',
            'address'               => 'nullable|string|min:2|max:100',
            'zip'                   => 'nullable|string|min:2|max:20',
            'company'               => 'nullable|string|min:2|max:50',
            'vatnr'                 => 'nullable|string|min:2|max:50',
        ]);

        DB::transaction(function () use ($request) {
            Clients::create([
                'name'              => $request->name,
                'reseller_username' => $request->reseller_username,
                'username'          => $request->email,
                'email'             => $request->email,
                'phone'             => $request->phone,
                'phone_valid'       => !empty($request->phone) ? 1 : 0,
                'password'          => \Hash::make($request->password),
                'country'           => $request->country,
                'city'              => $request->city,
                'created'           => date("Y-m-d H:i:s"),
                'address'           => $request->address,
                'company'           => $request->company,
                'person'            => $request->person,
                'zip'               => $request->zip,
                'vatnr'             => $request->vatnr,
            ]);
        });

        return redirect()->route('clients.index', ['status'=>'success',
                                                         'message' => 'Клиент успешно создан.']);
    }

    /**
     * Получение всех активных абонементов
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function activeSubscriptions($id)
    {
        $this->dealer = auth()->user();
        $client = Clients::find($id);

        $subscriptions = $client->abonements()
            ->where('expire', '>', Carbon::now()->toDateTimeString())
            ->get();

        $products = Products::where('type', 'subscription')
            ->where('promo', 0)
            ->where('enabled', 1)->get()->map(function ($product) {
                return collect($product)->merge([
                    'reseller_price' => $product->price * (100 - min($this->dealer->tarif_discont1, 100)) / 100
                ]);
            });

        return view('clients.subscription.index', [
            'subscriptions' => $subscriptions,
            'client'        => $client,
            'products'      => $products,
            'nav_item_css'  => 1
        ]);
    }

    /**
     * Получение всех не активных абонементов
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function expiredSubscriptions($id)
    {
        $this->dealer = auth()->user();
        $client = Clients::find($id);

        $subscriptions = $client->abonements()
            ->where('expire', '<', Carbon::now()->toDateTimeString())
            ->get();

        $products = Products::where('type', 'subscription')
            ->where('promo', 0)
            ->where('enabled', 1)->get()->map(function ($product) {
                return collect($product)->merge([
                    'reseller_price' => $product->price * (100 - min($this->dealer->tarif_discont1, 100)) / 100
                ]);
            });

        return view('clients.subscription.index', [
            'subscriptions' => $subscriptions,
            'client'        => $client,
            'products'      => $products,
            'nav_item_css'  => 1
        ]);
    }

    /**
     * Получение всех абонементов клиента
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subscriptions($id)
    {
        $this->dealer = auth()->user();
        $client = Clients::find($id);

        $products = Products::where('type', 'subscription')
            ->where('promo', 0)
            ->where('enabled', 1)->get()->map(function ($product) {
                return collect($product)->merge([
                    'reseller_price' => $product->price * (100 - min($this->dealer->tarif_discont1, 100)) / 100
                ]);
            });
        return view('clients.subscription.index', [
            'subscriptions' => $client->abonements()->get(),
            'client'        => $client,
            'products'      => $products,
            'nav_item_css'  => 1]);
    }

    /**
     * Получение всех клиентов через ajax
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_index_referals_ajax(Request $request)
    {
        if($request->ajax()){
            $dealer = auth()->user();
            $data = $dealer->referrals()->get()->map(function ($item){
                return collect($item)->merge([
                    'subscription'        => $item->abonements()->count(),
                    'subscritpion_active' => $item->abonements()
                        ->where('expire', '>', Carbon::now()->toDateTimeString())
                        ->count(),
                    'subscritpion_expend' => $item->abonements()
                        ->where('expire', '<', Carbon::now()->toDateTimeString())
                        ->count()]);
            });
            return response()->json(['data' => $data]);
        }
    }

    /**
     * Получение всех абонементов через ajax
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_index_abonements_ajax($id, Request $request)
    {
        if ($request->ajax()){
            $this->dealer = auth()->user();
            $this->client = Clients::find($id);
            $data = $this->client->abonements()->get()->map(function ($item) {
               return collect($item)->merge(['stream_server' => Settings::where('user_id', $item['id'])->where('var_name', 'settings.stream_server.value')->first()['var_value'],
                                             'timezone' => !is_null(Settings::where('user_id', $item['id'])->where('var_name', 'settings.timezone.value')->first()['var_value']) ?
                                                 Settings::where('user_id', $item['id'])->where('var_name', 'settings.timezone.value')->first()['var_value'] : '-'
               ]);
            });

            return response()->json(['data' => $data]);
        }
    }

    /**
     * Получение всех не активных абонементов через ajax
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_index_abonements_expired_ajax($id, Request $request)
    {
        if ($request->ajax()){
            $this->dealer = auth()->user();
            $this->client = Clients::find($id);
            $data = $this->client->abonements()->where('expire', '<', Carbon::now()->toDateTimeString())
                ->get()->map(function ($item) {
                return collect($item)->merge([
                    'stream_server' => Settings::where('user_id', $item['id'])->where('var_name', 'settings.stream_server.value')->first()['var_value'],
                    'timezone' => !is_null(Settings::where('user_id', $item['id'])->where('var_name', 'settings.timezone.value')->first()['var_value']) ?
                        Settings::where('user_id', $item['id'])->where('var_name', 'settings.timezone.value')->first()['var_value'] : '-'
                ]);
            });

            return response()->json(['data' => $data]);
        }
    }

    /**
     * Получение всех активных абонементов через ajax
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_index_abonements_active_ajax($id, Request $request)
    {
        if ($request->ajax()){
            $this->dealer = auth()->user();
            $this->client = Clients::find($id);
            $data = $this->client->abonements()->where('expire', '>', Carbon::now()->toDateTimeString())
                ->get()->map(function ($item) {
                    return collect($item)->merge([
                        'stream_server' => Settings::where('user_id', $item['id'])->where('var_name', 'settings.stream_server.value')->first()['var_value'],
                        'timezone' => !is_null(Settings::where('user_id', $item['id'])->where('var_name', 'settings.timezone.value')->first()['var_value']) ?
                            Settings::where('user_id', $item['id'])->where('var_name', 'settings.timezone.value')->first()['var_value'] : '-'
                    ]);
                });

            return response()->json(['data' => $data]);
        }
    }
}
