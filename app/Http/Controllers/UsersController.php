<?php

namespace App\Http\Controllers;

use App\Events\UserCreated;
use App\Events\UserUpdated;
use Illuminate\Support\Facades\Mail;
use App\Balance;
use App\Cart;
use App\Mail\SubscriptionBuyMail;
use App\Clients;
use App\Http\Controllers\Controller;
use App\OrderedProduct;
use App\Settings;
use App\Streamingserver;
use App\User;
use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Carbon\Carbon;


class UsersController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @return View
     */
    public function index(Request $request)
    {
        $dealer = auth()->user();
        $referals = $dealer->referrals()->get()->map(function ($item){
            return collect($item)->merge([
                'subscription'        => $item->abonements()->get(),
                'subscritpion_active' => $item->abonements()
                    ->where('expire', '>', Carbon::now()->toDateTimeString())
                    ->count(),
                'subscritpion_expend' => $item->abonements()
                    ->where('expire', '<', Carbon::now()->toDateTimeString())
                    ->count()
            ]);
        });

        return view('users.index', [
            'referals' =>$referals,
            'nav_item_css'=> 2
        ]);
    }


    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    public function edit($id)
    {
        $subscription = User::find($id);
        $subscription['timeshift'] = Settings::where('user_id', $id)->where('var_name', 'settings.timeshift.value')->first()['var_value'];
        $subscription['timezone'] = Settings::where('user_id', $id)->where('var_name', 'settings.timezone.value')->first()['var_value'];
        $subscription['bitratehd'] = Settings::where('user_id', $id)->where('var_name', 'settings.bitratehd.value')->first()['var_value'];
        $subscription['stream_server'] = Settings::where('user_id', $id)->where('var_name', 'settings.stream_server.value')->first()['var_value'];
        $subscription['broadcast_servers'] = Streamingserver::where('realisation', 'flussonic')
            ->where('distribution_protocol', 'hls')
            ->where('distribution_server', null)
            ->where('name', 'not like', '%sunduk.tv%')->get();
        $subscription['archivebitratesd'] = Settings::where('user_id', $id)->where('var_name', 'settings.archivebitratesd.value')->first()['var_value'];

        return view('clients.subscription.edit', ['subscription' => $subscription,
            'nav_item_css'=> 2]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'user_id'       => 'required|exists:App\User,id',
            'stream_server' => 'required|exists:App\Streamingserver,name',
        ]);

        DB::beginTransaction();
        $subscription = User::find($request->user_id);

        $subscription->update([
            'pass'         => $request->password,
            'protect_code' => $request->protect_code,
        ]);

        $timeshift = Settings::where('user_id', $request->user_id)->where('var_name', 'settings.timeshift.value')->first();
        $timeshift->update([
            'var_value' => $request->timeshift
        ]);

        $timezone = Settings::where('user_id', $request->user_id)->where('var_name', 'settings.timezone.value')->first();
        $timezone->update([
            'var_value' => $request->timezone
        ]);

//        $bitratehd = Settings::where('user_id', $request->user_id)->where('var_name', 'settings.bitratehd.value')->first();
//        $bitratehd->update([
//            'var_value' => $request->bitratehd
//        ]);
//
//        $archivebitratesd = Settings::where('user_id', $request->user_id)->where('var_name', 'settings.archivebitratesd.value')->first();
//        $archivebitratesd->update([
//            'var_value' => $request->archivebitratesd
//        ]);

        $stream_server = Settings::where('user_id', $request->user_id)->where('var_name', 'settings.stream_server.value')->first();
        $stream_server->update([
            'var_value' => $request->stream_server
        ]);

        DB::commit();

        return redirect()->route('subscription.edit', [
                                       'id'=>$request->user_id,
                                       'status'=>'success',
                                       'message' => 'Абонемент успешно изменён.'
        ]);
    }

    /**
     * Продление товара
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function extend($id){
        $this->dealer = auth()->user();
        $products = Products::where('type', 'subscription')
            ->where('promo', 0)
            ->where('enabled', 1)->get()->map(function ($product) {
                return collect($product)->merge([
                    'reseller_price' => $product->price * (100 - min($this->dealer->tarif_discont1, 100)) / 100
                ]);
            });
        $client = User::find($id)->client()->first();
        return view('users.edit', [
            'products'        =>$products,
            'client'          => $client,
            'subscription_id' => $id,
            'subscription'    => User::find($id),
            'referrals'       => $this->dealer->referrals()->get(),
            'balance'         => $this->dealer->balances()->where('type', 'user')
                ->where('currency', 'EUR')->get()->sum('amount'),
            'broadcast_servers' => Streamingserver::where('realisation', 'flussonic')
                ->where('distribution_protocol', 'hls')
                ->where('distribution_server', null)
                ->where('name', 'not like', '%sunduk.tv%')->get(),
            'nav_item_css'=> 2
        ]);
    }

    /**
     * Онбовление абонемента который продлили
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function renew($id, Request $request){
        $request->validate([
            'broadcast_server' => 'required|exists:App\Streamingserver,name|string',
            'client_id'        => 'required|exists:App\Clients,id|numeric',
            'products'         => 'required|exists:App\Products,id',
        ]);

        $this->dealer           = auth()->user();
        $this->balance_amount   = $this->dealer->balances()->where('type', 'user')->where('currency', 'EUR')->get()->sum('amount');
        $this->products         = Products::find($request->products);
        $this->reseller_price   = $this->products->sum('price') * (100 - min($this->dealer->tarif_discont1, 99)) / 100;

        if ($this->balance_amount < $this->reseller_price) {
            return redirect()->back()->withErrors('Не вашем счету не хватает денег');
        }

        DB::beginTransaction();
        $subscription = User::find($request->subscription);
        if (is_null($subscription)) {
            return redirect()->back()->withErrors('Абонемент не найден');
        }

        $subscription->update([
            'expire' => Carbon::now()->addDays($this->products->first()->period)->toDateTimeString(),
        ]);

        Balance::create([
            'type'      => 'user',
            'client_id' => $this->dealer->id,
            'amount'    => -$this->reseller_price,
            'currency'  => 'EUR',
            'initiator' => 'www.sunduk.tv',
//            'order_id' => $this->cart->id,
            'total'     => 14.50 //check
        ]);

        DB::commit();
        return redirect()->route('clients.index', [
            'status'  => 'success',
            'message' => 'Абонемент успешно продлён.'
        ]);
    }

    /**
     * Страница создания абонемента
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->dealer = auth()->user();
        $products = Products::where('type', 'subscription')
                            ->where('promo', 0)
                            ->where('enabled', 1)->get()->map(function ($product) {
               return collect($product)->merge([
                       'reseller_price' => $product->price * (100 - min($this->dealer->tarif_discont1, 100)) / 100
               ]);
                            });

        return view('users.create', [
          'products'  =>$products,
          'referrals' => $this->dealer->referrals()->get(),
          'balance'   => $this->dealer->balances()->where('type', 'user')
                                                  ->where('currency', 'EUR')->get()->sum('amount'),
          'broadcast_servers' => Streamingserver::where('realisation', 'flussonic')
              ->where('distribution_protocol', 'hls')
              ->where('distribution_server', null)
              ->where('name', 'not like', '%sunduk.tv%')->get(),
                      'nav_item_css'=> 2
        ]);
    }

    /**
     * Создание абонемента
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'broadcast_server' => 'required|exists:App\Streamingserver,name|string',
            'client_id'        => 'required|exists:App\Clients,id|numeric',
            'products'         => 'required|exists:App\Products,id',
        ]);

        //get_cart
        $this->client           = Clients::find($request->client_id); // client_id id кому покупается абонемент.
        $this->dealer           = auth()->user();
        $this->cart             = $this->dealer->cart()->where('status', 'cart')->first();
        $this->broadcast_server = $request->broadcast_server;
        $this->balance_amount   = $this->dealer->balances()->where('type', 'user')->where('currency', 'EUR')->get()->sum('amount');
        $this->products         = Products::find($request->products);
        $this->reseller_price   = $this->products->sum('price') * (100 - min($this->dealer->tarif_discont1, 99)) / 100;

        // Проверка. Если не хватает денег на счету, тогда редирект назад;
        if ($this->balance_amount < $this->reseller_price) {
            return redirect()->back()->withErrors('Не вашем счету не хватает денег');
        }

        DB::beginTransaction();

        if (is_null($this->cart)) {
            $this->cart = Cart::create([
                'created'     => Carbon::now()->toDateTimeString(),
                'updated'     => Carbon::now()->toDateTimeString(),
                'currency'    => 'EUR',
                'status'      => 'completed',
                'client_id'   => $this->dealer->id,
                'reseller_id' => $this->dealer->id,
                'referral_id' => $this->client->id,
                'paid'        => Carbon::now()->toDateTimeString(),
            ]);
        }

        //add_cart
        $this->products->each(function ($product){
            OrderedProduct::create([
                'order_id'   => $this->cart->id,
                'product_id' => $product['id'],
                'quantity'   => 1,
                'price'      => $product['price'],
                'currency'   => $this->cart->currency
            ]);
        });

        $this->cart->ordered_products()->get()->each(function ($ordered_product) {
            $product = $ordered_product->product()->first();

            // Создание абонемента
            $login = '';
            $length = 6;
            $i = 100;
            $generator = function ($length = 6) {
                $number = "";
                for ($i = 0; $i < $length; $i++) $number .= rand(0, 9);
                return $number;
            };

            while ($i && empty($login)) { //Проверка на существование login
                $login = $generator($length);

                if (User::where('login', $login)->first()) {
                    $login = '';
                }
                $i--;

                if ($i / 20 == ceil($i / 20) || $i < 5) {
                    $length++;
                }
            }

            $this->subscription = User::create([
                'client_id'    => $this->client->id,
                'name'         => $this->client->name,
                'login'        => $login,
                'created'      => Carbon::now()->toDateTimeString(),
                'expire'       => Carbon::now()->addDays($product['period'])->toDateTimeString(),
                'pass'         => $generator($length),
                'protect_code' => $generator($length),
            ]);

            $user_settings = json_decode(config('config.DEFAULT_USER_SETTINGS'), true);

            $user_settings['services.vod']             = 1;

            $user_settings['settings.stream_server.value'] = !empty($this->broadcast_server) ? $this->broadcast_server : 'Europe Central';

            foreach ($user_settings as $var_name => $var_value) {
                $settings            = new Settings();
                $settings->user_id   = $this->subscription->id;
                $settings->var_name  = $var_name;
                $settings->var_value = $var_value;
                $settings->save();
            }



        });

        //update_balance
       $balance = Balance::create([
            'type'      => 'user',
            'client_id' => $this->dealer->id,
            'amount'    => -$this->reseller_price,
            'currency'  => $this->cart->currency,
            'initiator' => 'www.sunduk.tv',
            'order_id' => $this->cart->id,
            'total'    => 14.50 //check
        ]);

        DB::commit();

        return redirect()->route('subscription.create', ['status' =>'success',
                                                               'message' => 'Абонемент успешно создан.']);
    }

    /**
     * Получение всех абонементов
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_index_subscription_ajax(Request $request)
    {
        $dealer = auth()->user();
        $this->sort_array = [];
        $referals = $dealer->referrals()->get()->each(function ($item){
            foreach ($item->abonements()->get() as $abonement) {
                $abonement['client'] = $abonement->client()->first()->name;
                $this->sort_array[]  = $abonement;
            }
        });
        return response()->json(['data' => $this->sort_array]);
    }
}
