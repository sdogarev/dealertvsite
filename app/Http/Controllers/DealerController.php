<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Dealer;
use Illuminate\Http\Request;


class DealerController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @return View
     */
    public function dealerAjaxBalance(Request $request)
    {
        if (!$request->ajax()){
           return abort(403);
        }
        $dealer  = auth()->user();
        $balance = $dealer->balances()->where('type', 'user')->where('currency', 'EUR')->get()->sum('amount');

        return response()->json(['data' => $balance]);
    }
}
