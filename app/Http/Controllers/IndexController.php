<?php

namespace App\Http\Controllers;

use App\Balance;
use App\Clients;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class IndexController extends Controller
{
    /**
     * Вывод информацию на главную страницу
     * @return array[]|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $dealer        = auth()->user();
        $count_clients = Clients::where('reseller_username', $dealer['email'])->count();
        $balance       = $dealer->balances()->where('type', 'user')->where('currency', 'EUR')->get()->sum('amount');
        return view('index', [
            'balance'          => $balance,
            'countclients'     => $count_clients,
            'discountreseller' => $dealer['tarif_discont1']
        ]);
    }
}
