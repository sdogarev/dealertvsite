<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Http\Controllers\Controller;
use App\Locations;
use App\Products;
use App\Settings;
use App\Streamingserver;
use App\TestUsers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Validation\Rule;


class TestUsersController extends Controller
{
    /**
     * Вывод всех тестовых абонементов
     * @return array[]|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $dealer = auth()->user();

        $maxTestUsersToReseller = !empty($dealer['max_test_user']) ? $dealer['max_test_user'] : 10;
        return view('demo.index', [
            'maxtestuser' => $maxTestUsersToReseller,
            'nav_item_css'=> 2
        ]);
    }

    /**
     * Вывод формы создание TestUser
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function create(Request $request)
    {
        $dealer = auth()->user();

        $dealer_abonements_count = $dealer->testusers()->get()->filter(function ($value, $key) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $value['created'])->isCurrentMonth();
        });

        $broadcast_server = Streamingserver::where('realisation', 'flussonic')
            ->where('distribution_protocol', 'hls')
            ->where('distribution_server', null)
            ->where('name', 'not like', '%sunduk.tv%')->get();
        return view('demo.create', [
            'broadcast_servers' => $broadcast_server,
            'nav_item_css'      => 2
        ]);
    }

    /**
     * Вывод формы обновления TestUser
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        $subscription = User::find($id);
        $subscription['broadcast_server'] = Streamingserver::where('realisation', 'flussonic')
        ->where('distribution_protocol', 'hls')
        ->where('distribution_server', null)
        ->where('name', 'not like', '%sunduk.tv%')->get();
        $subscription['timeshift']        = Settings::where('user_id', $id)->where('var_name', 'settings.timeshift.value')->first()['var_value'];
        $subscription['timezone']         = Settings::where('user_id', $id)->where('var_name', 'settings.timezone.value')->first()['var_value'];
        $subscription['bitratehd']        = Settings::where('user_id', $id)->where('var_name', 'settings.bitratehd.value')->first()['var_value'];
        $subscription['bitratesd']        = Settings::where('user_id', $id)->where('var_name', 'settings.bitratesd.value')->first()['var_value'];
        $subscription['stream_server']    = Settings::where('user_id', $id)->where('var_name', 'settings.stream_server.value')->first()['var_value'];
        $subscription['archivebitratesd'] = Settings::where('user_id', $id)->where('var_name', 'settings.archivebitratesd.value')->first()['var_value'];
        return view('demo.edit', [
            'subscription' => $subscription,
            'nav_item_css' => 2
        ]);
    }

    /**
     * Обновление TestUser
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {

        $validator = $request->validate([
            'broadcast_server' => 'required|exists:App\Streamingserver,name|string',
            'password'         => 'required|numeric|digits_between:6,6',
            'protect_code'     => 'required|numeric|digits_between:6,6',
            'timezone'         => 'required|numeric|min:-12|max:14',
            'timeshift'        => 'required|numeric|max:14',
            'bitratehd'    => 'required',
            'bitratesd'        => 'required',
//            'bitratevod'       => 'required',
        ]);

        DB::beginTransaction();
        $subscription = User::find($request->user_id);
        $subscription->update([
            'pass'         => $request->password,
            'protect_code' => $request->protect_code,
        ]);


        $timeshift = Settings::where('user_id', $request->user_id)->where('var_name', 'settings.timeshift.value')->first();
        $timeshift->update([
            'var_value' => $request->timeshift
        ]);

        $timezone = Settings::where('user_id', $request->user_id)->where('var_name', 'settings.timezone.value')->first();
        $timezone->update([
            'var_value' => $request->timezone
        ]);

        $bitratehd = Settings::where('user_id', $request->user_id)->where('var_name', 'settings.bitratehd.value')->first();
        $bitratehd->update([
            'var_value' => $request->bitratehd
        ]);

        $bitratesd = Settings::where('user_id', $request->user_id)->where('var_name', 'settings.bitratesd.value')->first();
        $bitratesd->update([
            'var_value' => $request->bitratesd
        ]);

        $stream_server = Settings::where('user_id', $request->user_id)->where('var_name', 'settings.stream_server.value')->first();
        $stream_server->update([
            'var_value' => $request->broadcast_server
        ]);
        DB::commit();
//
//        $stream_server = Settings::where('user_id', $request->user_id)->where('var_name', 'services.vod')->first();
//        $stream_server->var_value = 0;
//        $stream_server->save();
       return redirect()->route('demos.index', [
           'status'  => 'success',
           'message' => 'Тестовый абонемент изменён'
       ]);
    }

    /**
     * Создание тестового абонемента.
     *
     * @param Request $request
     * @return array[]|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $dealer = auth()->user();

        $dealer_abonements_count = $dealer->testusers()->get()->filter(function ($value, $key) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $value['created'])->isCurrentMonth();
        })->count();

        if( $dealer_abonements_count >= $dealer['max_test_user']) {
            return redirect()->back()->withErrors('Ваш лимит исчерпан');
        }
//
        //validate
        $validator = $request->validate([
            'broadcast_server' => 'required|exists:App\Streamingserver,name|string',
            'timezone'         => 'required|numeric|min:-12|max:14',
            'timeshift'        => 'required|numeric|max:14',
            'bitratehd'        => ['required', Rule::in(['Automatic', 'Standart', 'Economy'])],
            'bitratesd'        => ['required', Rule::in(['Standart', 'Economy'])],
//            'vod_bitrate'      => ['required', Rule::in(['Standart', 'Economy'])],

        ]);

        //create testuser
        do
        {
            $login = rand(000000, 999999);
            $user = User::where('login', $login)->get();
        }
        while(!$user->isEmpty());

        $generator = function ($length = 6) {
            $number = "";
            for ($i = 0; $i < $length; $i++) $number .= rand(0, 9);
            return $number;
        };
        $pass = $generator(6);

        DB::beginTransaction();

        $abonement = User::create([
            'client_id'    => $dealer->id,
            'name'         => $dealer->name,
            'login'        => $login,
            'pass'         => $pass,
            'protect_code' => $pass,
            'created'      => date('Y-m-d H:i:s'),
            'expire'       => Carbon::now()->addDays(config('config.DEMO_DAYS'))->toDateTimeString(),

        ]);
        $abonement_id = $abonement->id;

        // create user settings
        $user_settings = json_decode(config('config.DEFAULT_USER_SETTINGS'), true);

        $user_settings['services.vod']             = 0;
        $user_settings['settings.timezone.value']  = $request->timezone;
        $user_settings['settings.timeshift.value'] = $request->timeshift;
        $user_settings['settings.bitratehd.value'] = $request->bitratehd;
        $user_settings['settings.bitratesd.value'] = $request->bitratesd;

        $streamServerName = 'Europe 1';

        $geoContinentCode = geoip()->getLocation($_SERVER['REMOTE_ADDR']);
        if ($geoContinentCode && ($geoContinentCode['iso_code'] == 'NA' || $geoContinentCode['iso_code'] == 'SA')) {
            $user_settings['settings.stream_server.value'] = 'Europe Central';
            $streamServerName = 'Europe Central';
        }

        $user_settings['settings.stream_server.value'] = !empty($request->broadcast_server) ? $request->broadcast_server : 'Europe Central';

        foreach ($user_settings as $var_name => $var_value) {
            $settings            = new Settings();
            $settings->user_id   = $abonement_id;
            $settings->var_name  = $var_name;
            $settings->var_value = $var_value;
            $settings->save();
        }

        //create testuser
            TestUsers::create([
                'email'       => $dealer->email,
                'created'     => date('Y-m-d H:i:s'),
                'user_id'     => $abonement->login,
                'id_social'   => '',
                'location'    => !empty($dealer->location) && !empty($dealer->location()->first()) ? $dealer->location : '',
                'reseller_id' => $dealer->id
            ]);
        DB::commit();
        return redirect()->route('demos.index', [
            'status'  => 'success',
            'message' => 'Тестовый абонемент создан'
        ]);
    }

    /**
     * Список тестовых абонементов
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_index_demo_ajax(Request $request)
    {
        if ($request->ajax()){
            $dealer = auth()->user();
            $data = $dealer->testusers()->whereMonth('created', Carbon::now()->month)
                ->whereYear('created', Carbon::now()->year)->get()->map(function ($abonement) {
                    return $abonement->abonements()->first();
                });
            return response()->json(['data' => $data]);
        }
    }
}
