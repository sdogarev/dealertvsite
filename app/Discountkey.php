<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Discountkey extends Model
{
    protected $table = 'discountkeys';
    public $timestamps = false;

}
