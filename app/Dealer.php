<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Dealer extends Authenticatable
{
    use Notifiable;

    /**
     * @var string
     */
    protected $table = 'dealers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'apikey', 'phone', 'country', 'city'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function abonements()
    {
        return $this->hasMany('App\User', 'client_id');
    }

    public function referrals() {
        return $this->hasMany('App\Clients', 'reseller_username', 'username');
    }

    public function location()
    {
        return $this->hasOne('App\Locations', 'id', 'location');
    }

    public function balances()
    {
        return $this->hasMany('App\Balance', 'client_id');
    }

    public function testusers()
    {
        return $this->hasMany('App\TestUsers', 'reseller_id', 'id');
    }

    public function cart()
    {
        return $this->hasOne('App\Cart', 'client_id', 'id');
    }
}
