<?php

namespace App\Listeners;

use App\Clients;
use App\Events\UserCreated;
use App\Events\UserUpdated;
use App\Mail\SubscriptionBuyMail;
use App\Mail\SubscriptionExtendMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class UserEventSubscriber
{
    /**
     * Отправка письма об успешной покупке
     * @param UserCreated $event
     */
    public function handleUserCreated(UserCreated $event)
    {
        //        $clientEmail = Clients::find($event->user->client_id)->email;

        try {
            Mail::to('dikodax468@mail35.net')
                ->send(New SubscriptionBuyMail($event->user));
        } catch(\Exception $e) {
            $this->logErrorEmail($e);
            sleep(50);
            Mail::to('dikodax468@mail35.net')
                ->send(New SubscriptionBuyMail($event->user));
        }

    }

    /**
     * Отправка письма об успешном продление абонемента
     * @param UserUpdated $event
     */
    public function handleUserUpdated(UserUpdated $event)
    {
        //        $clientEmail = Clients::find($event->user->client_id)->email;

        try {
            Mail::to('dikodax468@mail35.net')
                ->send(New SubscriptionExtendMail($event->user));
       } catch(\Exception $e) {
            $this->logErrorEmail($e);
            sleep(50);
            Mail::to('dikodax468@mail35.net')
                ->send(New SubscriptionExtendMail($event->user));
       }
    }

    public function logErrorEmail($e)
    {
        Log::info('Ошибка отправки сообщения: '.$e->getMessage());
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\UserCreated',
            'App\Listeners\UserEventSubscriber@handleUserCreated'
        );

        $events->listen(
            'App\Events\UserUpdated',
            'App\Listeners\UserEventSubscriber@handleUserUpdated'
        );
    }

}
