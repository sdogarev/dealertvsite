<?php

namespace App\Listeners;

use App\Clients;
use App\Events\UserUpdated;
use App\Mail\SubscriptionExtendMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendExtendCompleteNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserUpdated  $event
     * @return void
     */
    public function handle(UserUpdated $event)
    {
//        $clientEmail = Clients::find($event->user->client_id)->id;
//        dd($clientEmail);
        Mail::to('dikodax468@mail35.net')
            ->send(New SubscriptionExtendMail($event->user));
    }
}
