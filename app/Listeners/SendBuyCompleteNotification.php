<?php

namespace App\Listeners;

use App\Clients;
use App\Events\UserCreated;
use App\Mail\SubscriptionBuyMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendBuyCompleteNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        $clientEmail = Clients::find($event->user->client_id)->email;
//        dd($clientEmail);
        Mail::to('dikodax468@mail35.net')
            ->send(New SubscriptionBuyMail($event->user));
    }
}
