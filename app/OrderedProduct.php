<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class OrderedProduct extends Model
{
    /**
     * @var string
     */
    protected $table = 'ordered_products';
    public $timestamps = false;

    protected $fillable = [
        'order_id', 'product_id', 'quantity', 'price', 'currency'
    ];

    public function products()
    {
        return $this->hasMany('App\Products', 'id', 'product_id');
    }

    public function product()
    {
        return $this->hasOne('App\Products', 'id', 'product_id');
    }
}
