<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Clients extends Model
{
    /**
     * @var string
     */
    protected $table = 'clients';
    public $timestamps = false;

    protected $fillable = [
        'reseller_username', 'name', 'username', 'email', 'phone', 'phone_valid', 'person',
        'password', 'phone_valid', 'country', 'city', 'created', 'zip'
    ];

    public function dealer()
    {
        return self::belongsTo('App\Dealer', 'reseller_username', 'username');
    }

    public function abonements()
    {
        return $this->hasMany('App\User', 'client_id');
    }

    public function location()
    {
        return $this->hasOne('App\Locations', 'id', 'country');
    }

}
