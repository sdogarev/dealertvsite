<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Order extends Model
{
    /**
     * @var string
     */
    protected $table = 'orders';
    public $timestamps = false;

    public function ordered_all_products()
    {
        return $this->hasMany('App\OrderedProduct', 'order_id', 'id');
    }
}
