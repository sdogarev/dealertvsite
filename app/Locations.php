<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Locations extends Model
{
    /**
     * @var string
     */
    protected $table = 'locations';
    public $timestamps = false;

}
