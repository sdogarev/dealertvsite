<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Streamingserver extends Model
{
    protected $table = 'streaming_servers';
    public $timestamps = false;
}
