<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
    <li class="sidebar-toggler-wrapper">
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="sidebar-toggler">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
    </li>
    <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
    <li class="start @if(1 == @$nav_item_css)active open @endif">
        <a href="javascript:;">
            <i class="icon-people"></i>
            <span class="title">Пользователи</span>
            <span class="selected"></span>
            <span class="arrow @if(1 == @$nav_item_css)open @endif"></span>
        </a>
        <ul class="sub-menu" style="@if(1 == @$nav_item_css) display:block @endif">
            <li class="{{parse_url(route('client.create'))['path'] == $_SERVER['REQUEST_URI'] ? 'active' : ''}}">
                <a href="{{ route("client.create") }}">
                    <i class="icon-user-follow"></i>
                    Добавить клиента</a>
            </li>
            <li class="{{parse_url(route('clients.index'))['path'] == $_SERVER['REQUEST_URI'] ? 'active' : ''}}">
                <a href="{{ route('clients.index') }}">
                    <i class="icon-people"></i>
                    Список клиентов</a>
            </li>
        </ul>
    </li>
    <li class="@if(2 == @$nav_item_css) active open @endif">
        <a href="javascript:;">
            <i class="icon-rocket"></i>
            <span class="title">Абонементы</span>
            <span class="arrow @if(2 == @$nav_item_css)open @endif"></span>
        </a>
        <ul class="sub-menu" style="@if(2 == @$nav_item_css) display:block @endif">
            <li class="{{parse_url(route('subscriptions.index'))['path'] == $_SERVER['REQUEST_URI'] ? 'active' : ''}}">
                <a href="{{ route('subscriptions.index') }}">
                    Список абонементов</a>
            </li>
            <li class="{{parse_url(route('subscription.create'))['path'] == $_SERVER['REQUEST_URI'] ? 'active' : ''}}">
                <a href="{{ route('subscription.create') }}">
                    Создать абонемент</a>
            </li>
            <li class="{{parse_url(route('demos.index'))['path'] == $_SERVER['REQUEST_URI'] ? 'active' : ''}}">
                <a href="{{ route('demos.index') }}">
                    Тестовые абонементы</a>
            </li>
            <li class="{{parse_url(route('demo.create'))['path'] == $_SERVER['REQUEST_URI'] ? 'active' : ''}}">
                <a href="{{ route('demo.create') }}">
                    Создать тестовый абонемент</a>
            </li>
        </ul>
    </li>
    <li class="@if(3 == @$nav_item_css) active open @endif">
        <a href="javascript:;">
            <i class="icon-wallet"></i>
            <span class="title">Пополнение баланса</span>
            <span class="arrow @if(3 == @$nav_item_css)open @endif"></span>
        </a>
        <ul class="sub-menu" style="@if(3 == @$nav_item_css) display:block @endif">
            <li>
                <a href="ecommerce_index.html">
                    <i class="icon-home"></i>
                    Dashboard</a>
            </li>
            <li>
                <a href="ecommerce_orders.html">
                    <i class="icon-basket"></i>
                    Orders</a>
            </li>
            <li>
                <a href="ecommerce_orders_view.html">
                    <i class="icon-tag"></i>
                    Order View</a>
            </li>
            <li>
                <a href="ecommerce_products.html">
                    <i class="icon-handbag"></i>
                    Products</a>
            </li>
            <li>
                <a href="ecommerce_products_edit.html">
                    <i class="icon-pencil"></i>
                    Product Edit</a>
            </li>
        </ul>
    </li>
</ul>
