@extends('layouts.app')
@section('title', __('demo.page_title.app_header_create_demo'))
@section('content')
    <?php if($_SERVER['REQUEST_METHOD'] == "POST") {
        var_dump($_POST);
    }?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title" style="margin-bottom: 35px;">
                {{ __('demo.page_title.app_header_create_demo') }}
            </h3>
            <div class="page-bar" style="display: none">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Data Tables</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Editable Datatables</a>
                    </li>
                </ul>
                <div class="page-toolbar">

                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">

                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box">
                        <div class="portlet-body form">
                            <form class="form-horizontal" role="form" action="{{ route('demo.store') }}" method="post">
                                @csrf
                                <input type="hidden" name="reseller_username" value="<?php echo Auth::user()->email ?>">
                                <input type="hidden" name="phone_valid" value="0">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">{{ __('demo.demo_create_label_1') }}</label>
                                        <div class="col-md-9">
                                        <select name="broadcast_server" style="width: 30%" class="form-control input-lg">
                                            @foreach($broadcast_servers as $broadcast_server)
                                            <option value="{{ $broadcast_server['name'] }}">{{ $broadcast_server['name'] }}</option>
                                            @endforeach
                                        </select>
                                            @error('broadcast_server')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('demo.demo_create_label_2') }}</label>
                                        <div class="col-md-9">
                                            <select class="form-control input-lg" style="width: 30%" name="timezone">
                                                <option value="13">+13</option>
                                                <option value="12">+12</option>
                                                <option value="11">+11</option>
                                                <option value="10">+10</option>
                                                <option value="9">+9</option>
                                                <option value="8">+8</option>
                                                <option value="7">+7</option>
                                                <option value="6">+6</option>
                                                <option value="5">+5</option>
                                                <option value="4">+4</option>
                                                <option value="3">+3</option>
                                                <option value="2" selected="">+2</option>
                                                <option value="1">+1</option>
                                                <option value="0">0</option>
                                                <option value="-1">-1</option>
                                                <option value="-2">-2</option>
                                                <option value="-3">-3</option>
                                                <option value="-4">-4</option>
                                                <option value="-5">-5</option>
                                                <option value="-6">-6</option>
                                                <option value="-7">-7</option>
                                                <option value="-8">-8</option>
                                                <option value="-9">-9</option>
                                                <option value="-10">-10</option>
                                                <option value="-11">-11</option>
                                            </select>
                                            @error('timezone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('demo.demo_create_label_3') }}</label>
                                        <div class="col-md-9">
                                            <input type="number" name="timeshift" style="width: 30%" class="form-control input-lg" value="0">
                                            @error('timeshift')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('demo.demo_create_label_4') }}</label>
                                        <div class="col-md-9">
                                            <select class="form-control input-lg" style="width: 30%" name="bitratehd">
                                                <option value="Automatic">Automatic</option>
                                                <option value="Standart">Standart</option>
                                                <option value="Economy" selected="">Economy</option>
                                            </select>
                                            @error('bitratehd')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('demo.demo_create_label_5') }}</label>
                                        <div class="col-md-9">
                                            <select class="form-control input-lg" style="width: 30%" name="bitratesd">
                                                <option value="Standart">Standart</option>
                                                <option value="Economy" selected="">Economy</option>
                                            </select>
                                            @error('bitratesd')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        @error('vod_bitrate')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <label class="col-md-3 control-label">{{ __('demo.demo_create_label_6') }}</label>
                                        <div class="col-md-9">
                                            <input type="number" name="vod_bitrate" style="width: 30%" class="form-control input-lg" value="8000">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions right1">
                                    <a href="{{ url()->previous() }}" class="btn default">{{ __('demo.demo_btn_cancel') }}</a>
                                    <button type="submit" class="btn green">{{ __('demo.demo_btn_submit') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection
