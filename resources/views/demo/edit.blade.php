@extends('layouts.app')
@section('title',  __('demo.page_title.app_header_edit_demo'))
@section('content')
    <?php if($_SERVER['REQUEST_METHOD'] == "POST") {
        var_dump($_POST);
    }?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title" style="margin-bottom: 35px;">
                {{ __('demo.page_title.app_header_edit_demo') }}
            </h3>
            <div class="page-bar" style="display: none">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Data Tables</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Editable Datatables</a>
                    </li>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                            Actions <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="#">Action</a>
                            </li>
                            <li>
                                <a href="#">Another action</a>
                            </li>
                            <li>
                                <a href="#">Something else here</a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="#">Separated link</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                @endif
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                        <div class="portlet-body form">
                            <form class="form-horizontal" role="form" action="{{route('demo.update', ['id' => $subscription['id']])}}" method="post">
                                @csrf
                                <input type="hidden" name="user_id" value="{{ $subscription['id'] }}">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('demo.demo_edit_label_1') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" style="width: 30%" name="password" class="form-control input-lg" value="{{ $subscription['pass'] }}">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('demo.demo_edit_label_2') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" style="width: 30%" name="protect_code" class="form-control input-lg" value="{{ $subscription['protect_code'] }}">
                                            @error('protect_code')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('demo.demo_edit_label_3') }}</label>
                                        <div class="col-md-9">
                                            <select name="broadcast_server" style="width: 30%" class="form-control input-lg">
                                                <option value="{{ $subscription['stream_server'] }}">{{ $subscription['stream_server'] }}</option>
                                                @foreach($subscription['broadcast_server'] as $broadcast_server)
                                                    <option value="{{ $broadcast_server['name'] }}">{{ $broadcast_server['name'] }}</option>
                                                @endforeach
                                            </select>
                                            @error('broadcast_server')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Часовая зона</label>
                                        <div class="col-md-9">
                                            <input type="text" name="timezone" style="width: 30%" class="form-control input-lg" value="{{ $subscription['timezone'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Cдвиг времени</label>
                                        <div class="col-md-9">
                                            <input type="text" name="timeshift" style="width: 30%" class="form-control input-lg" value="{{ $subscription['timeshift'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('demo.demo_create_label_4') }}</label>
                                        <div class="col-md-9">
                                            <select class="form-control input-lg" style="width: 30%" name="bitratehd">
                                                <option value="Automatic" @if($subscription['bitratehd'] == "Automatic") selected @endif>Automatic</option>
                                                <option value="Standart" @if($subscription['bitratehd'] == "Standart") selected @endif>Standart</option>
                                                <option value="Economy" @if($subscription['bitratehd'] == "Economy") selected @endif>Economy</option>
                                            </select>
                                            @error('bitratehd')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('demo.demo_create_label_5') }}</label>
                                        <div class="col-md-9">
                                            <select class="form-control input-lg" style="width: 30%" name="bitratesd">
                                                <option value="Standart" @if($subscription['bitratesd'] == "Standart") selected @endif>Standart</option>
                                                <option value="Economy" @if($subscription['bitratesd'] == "Economy") selected @endif>Economy</option>
                                            </select>
                                            @error('bitratehd')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

{{--                                    <div class="form-group">--}}
{{--                                        <label class="col-md-3 control-label">Bitrate Видеотека</label>--}}
{{--                                        <div class="col-md-9">--}}
{{--                                            <input type="text" name="company" class="form-control input-lg" value="{{ $subscription['protect_code'] }}">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                </div>
                                <div class="form-actions right1">
                                    <a href="{{ url()->previous() }}" class="btn default">{{ __('demo.demo_btn_cancel') }}</a>
                                    <button type="submit" class="btn green">{{ __('demo.demo_btn_submit') }}</button>
                                </div>
                            </form>
                        </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection
