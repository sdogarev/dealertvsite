@extends('layouts.app')
@section('title', __('demo.page_title.app_header_index_demo'))
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title" style="margin-bottom: 35px;">
                {{  __('demo.page_title.app_header_index_demo') }}
            </h3>
            <div class="page-bar" style="display: none">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Data Tables</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Editable Datatables</a>
                    </li>
                </ul>
                <div class="page-toolbar">
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    @if(!empty($_GET['status']) && $_GET['status'] == 'success')
                        <div class="note note-success">
                            {{ $_GET['message'] }}
                        </div>
                    @endif
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                {{ __('demo.demo_index_message', ['maxtestuser' => $maxtestuser]) }}
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse">
                                </a>
                                <a href="#portlet-config" data-toggle="modal" class="config">
                                </a>
                                <a href="javascript:;" class="reload">
                                </a>
                                <a href="javascript:;" class="remove">
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="btn-group">
                                            <a type="submit" href="{{ route('demo.create', ['client_ip' => $_SERVER['REMOTE_ADDR']]) }}" id="sample_editable_1_new" class="btn green">
                                                {{ __('demo.demo_index_btn') }} <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="url_demo_edit" value="{{ route('demo.edit', ['id' => 'XXX']) }}">
                            <input type="hidden" id="url_ajax" value="{{ route('clients.demo.ajax.index') }}">
                            <table class="table table-striped table-hover table-bordered" id="table_demo_index">
                                <thead>
                                <tr>
                                    <th>
                                        {{ __('demo.demo_index_table_header_1') }}
                                    </th>
                                    <th>
                                        {{ __('demo.demo_index_table_header_2') }}
                                    </th>
                                    <th>
                                        {{ __('demo.demo_index_table_header_3') }}
                                    </th>
                                    <th>
                                        {{ __('demo.demo_index_table_header_4') }}
                                    </th>
                                    <th>
                                        {{ __('demo.demo_index_table_header_5') }}
                                    </th>
                                    <th>
                                        {{ __('demo.demo_index_table_header_6') }}
                                    </th>
                                    {{--                                <th>--}}
                                    {{--                                    Delete--}}
                                    {{--                                </th>--}}
                                </tr>
                                </thead>
                                <tbody>
{{--                                @foreach ($testusers as $testuser)--}}
{{--                                    <tr>--}}
{{--                                        <td>--}}
{{--                                            <a href="{{ route('demo.edit', ['id' => $testuser->id]) }}">{{ $testuser->login }}</a>--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            {{ $testuser->pass }}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            {{ $testuser->protect_code }}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            {{ $testuser->created }}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            {{ $testuser->expire }}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            @if((int) (0 > (strtotime($testuser->expire) - time())))--}}
{{--                                                {{ __('demo.demo_index_status_2') }}--}}
{{--                                            @else--}}
{{--                                                {{ __('demo.demo_index_status_1') }}--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
{{--                                @endforeach--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection
