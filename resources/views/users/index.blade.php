@extends('layouts.app')
@section('title', __('subscription.page_title.app_header_sub'))
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title" style="margin-bottom: 35px;">
                {{ __('subscription.page_title.app_header_sub') }}
            </h3>
            <div class="page-bar" style="display: none">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Data Tables</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Editable Datatables</a>
                    </li>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <button  style="display: none" type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                            Actions <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="#">Action</a>
                            </li>
                            <li>
                                <a href="#">Another action</a>
                            </li>
                            <li>
                                <a href="#">Something else here</a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="#">Separated link</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a href="{{ route('subscription.create') }}" id="sample_editable_1_new" class="btn green">
                                                Создать абонемент <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="btn-group pull-right">
                                            <button style="display: none" class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;">
                                                        Print </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        Save as PDF </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        Export to Excel </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="url_ajax" value="{{ route('subscriptions.abonements.ajax.index') }}">
                            <input type="hidden" id="url_clients_edit" value="{{ route('client.edit', ['id' => 'XXX']) }}">
                            <input type="hidden" id="url_client_sub_edit" value="{{ route('subscription.edit', ['id' => 'XXX']) }}">
                            <input type="hidden" id="url_sub_edit" value="{{ route('subscription.extend', ['id' => 'XXX']) }}">
                            <table class="table table-striped table-hover table-bordered" id="table_sub_index">
                                <thead>
                                <tr>
                                    <th>
                                        {{ __('subscription.subscription_index_table_header_1') }}
                                    </th>
                                    <th>
                                        {{ __('subscription.subscription_index_table_header_2') }}
                                    </th>
                                    <th>
                                        {{ __('subscription.subscription_index_table_header_3') }}
                                    </th>
                                    <th>
                                        {{ __('subscription.subscription_index_table_header_4') }}
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
{{--                                @foreach ($referals as $referal)--}}
{{--                                    <tr>--}}
{{--                                        <td>--}}
{{--                                            <a href="{{ route('clients.edit', ['id' => $referal['id']]) }}">{{$referal['name']}}</a>--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            <a href="{{ route('clients.subscriptions.index', ['id' => $referal['id']]) }}">{{$referal['subscription']}}</a>--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            <a href="{{ route('clients.subscriptions.active.index', ['id' => $referal['id']]) }}">{{$referal['subscritpion_active']}}</a>--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            <a href="{{ route('clients.subscriptions.expired.index', ['id' => $referal['id']]) }}">{{$referal['subscritpion_expend']}}</a>--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            {{$referal['expire']}}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            {{$referal['login']}}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            {{$referal['notifyexpiry']}}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            1--}}
{{--                                            {{!empty($referal['location']) ? $referal['location'] : '-'}}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            <a class="btn btn-primary btn-modal" data-idabonement="{{ $subscription['id'] }}">Продлить</a>--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
{{--                                @endforeach--}}
                                </tbody>
                            </table>
                        </div>

                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection
