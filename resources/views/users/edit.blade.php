@extends('layouts.app')
@section('title', __('subscription.page_title.app_header_extend_sub'))
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title" style="margin-bottom: 35px;">
                {{ __('subscription.page_title.app_header_extend_sub') }}
            </h3>
            <div class="page-bar" style="display: none">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Data Tables</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Editable Datatables</a>
                    </li>
                </ul>
                <div class="page-toolbar">

                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box">
                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="note note-danger">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif
                        <div class="note note-danger" style="display: none">
                            {{ __('subscription.order_note_2') }} <a href="#">{{ __('subscription.link_payment') }}</a>
                        </div>
                        @if (!empty($_GET['status']) && $_GET['status'] == 'success')
                            <div class="note note-success">
                                {{ __('subscription.order_note_success') }}
                            </div>
                        @endif
                        <div class="note note-success">
                            {{ __('subscription.order_title_2') }} € <span class="balance">{{ $balance }}</span>. {{ __('subscription.order_title_3') }} € <span id="sum_price_product">0.00</span>
                        </div>

                        <div class="note note-success">
                            <b>Клиент:</b> {{ $subscription->name }} | <b>Логин:</b> {{ $subscription->login }} | <b>Пароль:</b> {{ $subscription->pass }}
                        </div>
                        <div class="portlet-body form">
                            <form class="form-horizontal" role="form" action="{{ route('subscription.renew', ['id' => $subscription_id]) }}" method="post">
                                @csrf
                                <input type="hidden" name="reseller_username" value="<?php echo Auth::user()->email ?>">
                                <input type="hidden" name="subscription" value="{{ $subscription_id }}">
                                <input type="hidden" name="phone_valid" value="0">
                                <div class="form-body">
                                    <div class="form-group">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-9">
                                            <label>{{ __('subscription.order_title_4') }}</label>
                                            <div class="input-group">
                                                @foreach($products as $product)
                                                    <div class="icheck-inline">
                                                        <label>
                                                            <input type="checkbox" name="products[]" value="{{$product['id']}}" class="icheck checkbox_price" data-price_product="{{$product['reseller_price']}}"> {{ $product['name'] }} <span style="text-decoration: underline; color: #32a852">(€ {{ $product['reseller_price'] }}) </span>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('subscription.order_title_5') }}</label>
                                        <div class="col-md-9">
                                            <select name="client_id" style="width: 30%" class="form-control input-lg">
                                                <option value="{{ auth()->user()['id'] }}">{{ __('subscription.order_select') }}</option>
                                                @foreach($referrals as $referral)
                                                    <option value="{{ $referral['id'] }}" {{ $client['id'] == $referral['id'] ? 'selected' : '' }}>{{ $referral['username'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ __('subscription.order_broadcast_server') }}</label>
                                            <div class="col-md-9">
                                                <select name="broadcast_server" style="width: 30%" class="form-control input-lg">
                                                    @foreach($broadcast_servers as $broadcast_server)
                                                        <option value="{{ $broadcast_server->name }}">{{ $broadcast_server->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right1">
                                        <a href="{{ url()->previous() }}" class="btn default">{{ __('subscription.order_btn_cancel') }}</a>
                                        <button type="submit" id="payment" class="btn green">{{ __('subscription.order_btn_submit') }}</button>
                                    </div>
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
    <script type="text/javascript">
        $(function() {
            const sum_price_products = $('#sum_price_product'); //Div блок куда выводится сумма всех продуктов
            const balance            = $('.balance').text();
            const noteDanger         = $('.note-danger'); //Блок с ошибкой о нехватке денег
            const btnPayment         = $('#payment');
            let sum                  = 0;
            $('.checkbox_price').click(function () {
                let goods = [0]; //Товары

                //Добавление в массив выбранных элементов
                $('.checkbox_price:checked').each(function(){
                    goods.push($(this).data('price_product'));
                });

                //Получаем сумму товаров
                sum = goods.reduce(function(sum, current) {
                    return sum + current;
                });

                sum_price_products.html(sum.toFixed(2)); //Вывод в блок sum_price_products сумму

                //Если на баланс не хватает денег, вырубаем кнопку submit и выводим ошибку
                if (balance > sum) {
                    noteDanger.hide();
                    btnPayment.prop('disabled', false);
                } else {
                    noteDanger.show();
                    btnPayment.prop('disabled', true);
                }
            });
        });

    </script>
    <!-- END QUICK SIDEBAR -->
@endsection
