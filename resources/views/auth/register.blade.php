<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    {{--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">--}}
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/font-awesome/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/bootstrap/bootstrap.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/uniform/uniform.default.css') }}" rel="stylesheet" type="text/css"/>
    {{--    <link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>--}}
<!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ asset('css/components.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/plugins.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/login.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/layout.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/darkblue.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="index.html">
        <img src="../../assets/admin/layout/img/logo-big-white.png" style="height: 17px;" alt=""/>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
<!-- BEGIN REGISTRATION FORM -->
<form class="register-form" action="{{ route('register') }}" method="post" style="display: block !important;">
    @csrf
    <input type="hidden" value="{{ $apikey }}" name="apikey">
    <div class="form-title">
        <span class="form-title">Регистрация</span>
    </div>
    <p class="hint">
        Введите ваши персональные данные:
    </p>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Имя, Фамилия</label>
        <input class="form-control placeholder-no-fix @error('name') is-invalid @enderror" type="text" placeholder="Имя, Фамилия" value="{{ old('name') }}" required autocomplete="name" autofocus name="name"/>
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Email</label>
        <input class="form-control placeholder-no-fix @error('email') is-invalid @enderror" type="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" name="email"/>
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Пароль</label>
        <input class="form-control placeholder-no-fix @error('password') is-invalid @enderror" type="password" placeholder="Пароль" value="{{ old('password') }}" required autocomplete="password" name="password"/>
        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Повторите пароль</label>
        <input class="form-control " type="password" placeholder="Повторите пароль" value="{{ old('password') }}" required autocomplete="new-password" value="{{ old('password_confirmation') }}" name="password_confirmation"/>
    </div>
    @error('password_confirmation')
    <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Телефон</label>
        <input class="form-control placeholder-no-fix" type="tel" value="{{ old('phone') }}" placeholder="Телефон" name="phone"/>
    </div>
    @error('phone')
    <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Страна</label>
        <select name="country" class="form-control">
            <option value="">Страна</option>
            @foreach($countries as $country)
            <option value="{{ $country->iso_3166_1_alpha2 }}">{{ $country->name }}</option>
            @endforeach
        </select>
    </div>
    @error('country')
    <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Город</label>
        <input class="form-control placeholder-no-fix" type="text" value="{{ old('city') }}" placeholder="Город" name="city"/>
    </div>
{{--    <p class="hint">--}}
{{--        Enter your account details below:--}}
{{--    </p>--}}
{{--    <div class="form-group">--}}
{{--        <label class="control-label visible-ie8 visible-ie9">Username</label>--}}
{{--        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username"/>--}}
{{--    </div>--}}
{{--    <div class="form-group">--}}
{{--        <label class="control-label visible-ie8 visible-ie9">Password</label>--}}
{{--        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password"/>--}}
{{--    </div>--}}
{{--    <div class="form-group">--}}
{{--        <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>--}}
{{--        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="rpassword"/>--}}
{{--    </div>--}}
{{--    <div class="form-group margin-top-20 margin-bottom-20">--}}
{{--        <label class="check">--}}
{{--            <input type="checkbox" name="tnc"/>--}}
{{--            <span class="loginblue-font">I agree to the </span>--}}
{{--            <a href="javascript:;" class="loginblue-link">Terms of Service</a>--}}
{{--            <span class="loginblue-font">and</span>--}}
{{--            <a href="javascript:;" class="loginblue-link">Privacy Policy </a>--}}
{{--        </label>--}}
{{--        <div id="register_tnc_error">--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="form-actions">
        <a href="{{ route('login') }}" id="register-back-btn" class="btn btn-default">Назад</a>
        <button type="submit" id="register-submit-btn" class="btn btn-primary uppercase pull-right">Создать</button>
    </div>
</form>
</div>
<div class="copyright hide">
    2014 © Metronic. Admin Dashboard Template.
</div>
<!-- END REGISTRATION FORM -->
<script src="{{ asset('js/plugins/respond.min.js') }}"></script>
<script src="{{ asset('js/plugins/excanvas.min.js') }}"></script>
<script src="{{ asset('js/metronic.js') }}"></script>
<script src="{{ asset('js/layout.js') }}"></script>
<script src="{{ asset('js/login.js') }}"></script>
<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Login.init();
        Demo.init();
    });
</script>
</body>
</html>
