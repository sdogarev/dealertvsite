@extends('layouts.app')
@section('title', __('clients.page_title.app_header_create_clients'))
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title" style="margin-bottom: 35px;">
                {{ __('clients.page_title.app_header_create_clients') }}
            </h3>
            <div class="page-bar" style="display:none;">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Data Tables</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Editable Datatables</a>
                    </li>
                </ul>
                <div class="page-toolbar">

                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">

                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box">
                        <div class="portlet-body form">
                            <form class="form-horizontal" role="form" action="{{route('client.store')}}" method="post">
                                @csrf
                                <input type="hidden" name="reseller_username" value="<?php echo Auth::user()->email ?>">
                                <input type="hidden" name="phone_valid" value="0">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><span style="color: red">* </span>{{ __('clients.clients_сreate_label_1') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" name="name" style="width: 30%;" class="form-control input-lg" value="">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><span style="color: red">* </span>{{ __('clients.clients_сreate_label_2') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" name="email" style="width: 30%;" class="form-control input-lg" value="">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><span style="color: red">* </span>{{ __('clients.clients_сreate_label_3') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" style="width: 30%;" name="password" class="form-control input-lg">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><span style="color: red">* </span>{{ __('clients.clients_сreate_label_4') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" style="width: 30%;" name="password_confirmation" class="form-control input-lg">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><span style="color: red">* </span>{{ __('clients.clients_сreate_label_5') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" name="phone" style="width: 30%;" class="form-control input-lg" value="">
                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><span style="color: red">* </span>{{ __('clients.clients_сreate_label_6') }}</label>
                                        <div class="col-md-9">
                                            <select name="country" style="width: 30%;" class="form-control input-lg">
                                                <option value="0">Страна</option>
                                                @foreach($countries as $country)
                                                    <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                                                @endforeach
                                            </select>
                                            @error('country')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_сreate_label_7') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" style="width: 30%;" name="city" class="form-control input-lg" value="">
                                            @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_сreate_label_8') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" style="width: 30%;" name="address" class="form-control input-lg" value="">
                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_сreate_label_9') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" style="width: 30%;" name="zip" class="form-control input-lg" value="">
                                            @error('zip')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-9">
                                            <select name="person" style="width: 30%;" class="form-control input-lg">
                                                <option value="individual" selected>Физическое лицо</option>
                                                <option value="legal">Юридическое лицо</option>
                                            </select>
                                            @error('person')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_сreate_label_10') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" style="width: 30%;" name="company" class="form-control input-lg" value="">
                                            @error('company')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_сreate_label_11') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" style="width: 30%;" name="vatnr" class="form-control input-lg" value="">
                                            @error('vatnr')
                                            <div class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions right1">
                                    <a href="{{ url()->previous() }}" class="btn default">{{ __('clients.clients_btn_cancel') }}</a>
                                    <button type="submit" class="btn green">{{ __('clients.clients_btn_submit') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection
