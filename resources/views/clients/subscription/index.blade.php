@extends('layouts.app')
@section('title', __('clients.page_title.app_header_index_sub_clients'))
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
{{--            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">--}}
{{--                <div class="modal-dialog">--}}
{{--                    <div class="modal-content">--}}
{{--                        <div class="modal-header">--}}
{{--                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>--}}
{{--                            <h4 class="modal-title">Modal title</h4>--}}
{{--                        </div>--}}
{{--                        <div class="modal-body">--}}
{{--                            <input type="hidden" class="sub" data-sub="qw" value="">--}}
{{--                            <form class="form-horizontal" role="form" action="" method="post">--}}
{{--                                @csrf--}}
{{--                                <input type="hidden" name="reseller_username" value="<?php echo Auth::user()->email ?>">--}}
{{--                                <input type="hidden" name="phone_valid" value="0">--}}
{{--                                <div class="form-body">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <div class="col-md-3"></div>--}}
{{--                                        <div class="col-md-9">--}}
{{--                                            <label>Выбор абонемента</label>--}}
{{--                                            <div class="input-group">--}}
{{--                                                @foreach($products as $product)--}}
{{--                                                    <div class="icheck-inline">--}}
{{--                                                        <label>--}}
{{--                                                            <input type="hidden" id="order_quantity" name="product[{{$product['id']}}]" value="1">--}}
{{--                                                            <input type="checkbox" name="products[]" value="{{$product['id']}}" class="icheck checkbox_price" data-price_product="{{$product['reseller_price']}}"> {{ $product['name'] }} <span style="text-decoration: underline; color: #32a852">(€ {{ $product['reseller_price'] }}) </span>--}}
{{--                                                    </div>--}}
{{--                                                @endforeach--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                        </div>--}}
{{--                        <div class="modal-footer">--}}
{{--                            <button type="submit" class="btn green">Submit</button>--}}
{{--                            <button type="button" class="btn default" data-dismiss="modal">Close</button>--}}
{{--                        </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                    <!-- /.modal-content -->--}}
{{--                </div>--}}
{{--                <!-- /.modal-dialog -->--}}
{{--            </div>--}}
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title" style="margin-bottom: 35px;">
                {{  __('clients.page_title.app_header_index_sub_clients') }}
            </h3>
            <div class="page-bar" style="display: none">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Data Tables</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Editable Datatables</a>
                    </li>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                            Actions <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="#">Action</a>
                            </li>
                            <li>
                                <a href="#">Another action</a>
                            </li>
                            <li>
                                <a href="#">Something else here</a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="#">Separated link</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                {{ $client['name'] }}
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse">
                                </a>
                                <a href="#portlet-config" data-toggle="modal" class="config">
                                </a>
                                <a href="javascript:;" class="reload">
                                </a>
                                <a href="javascript:;" class="remove">
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a href="{{ route('subscription.create') }}" id="sample_editable_1_new" class="btn green">
                                                {{ __('clients.clients_subscription_index_btn_1') }} <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="btn-group pull-right">
                                            <button style="display: none" class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;">
                                                        Print </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        Save as PDF </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        Export to Excel </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php  $q = explode('/', parse_url($_SERVER['REQUEST_URI'])['path']); ?>
                            <input type="hidden" id="client_id" value="{{ $client['id'] }}">
                            <input type="hidden" id="url_clients_sub_edit" value="{{ route('subscription.edit', ['id' => 'XXX']) }}">
                            <input type="hidden" id="url_sub_edit" value="{{ route('subscription.extend', ['id' => 'XXX']) }}">
                            <input type="hidden" id="ajax_abonements_expired" value="{{ route('clients.abonements.expired.ajax.index', ['id' => 'XXX']) }}">
                            <input type="hidden" id="ajax_abonements" value="{{ route('clients.abonements.ajax.index', ['id' => 'XXX']) }}">
                            <input type="hidden" id="ajax_abonements_active" value="{{ route('clients.abonements.active.ajax.index', ['id' => 'XXX']) }}">
                            <input type="hidden" id="uri_check" value="<?php echo end($q)?>">
                            <table class="table table-striped table-hover table-bordered" id="table_clients_sub_index">
                                <thead>
                                <tr>
                                    <th>
                                        {{ __('clients.clients_subscription_index_table_header_1') }}
                                    </th>
                                    <th>
                                        {{ __('clients.clients_subscription_index_table_header_2') }}
                                    </th>
                                    <th>
                                        {{ __('clients.clients_subscription_index_table_header_3') }}
                                    </th>
                                    <th>
                                        {{ __('clients.clients_subscription_index_table_header_4') }}
                                    </th>
                                    <th>
                                        {{ __('clients.clients_subscription_index_table_header_5') }}
                                    </th>
                                    <th>
                                        {{ __('clients.clients_subscription_index_table_header_6') }}
                                    </th>
                                    <th>
                                        {{ __('clients.clients_subscription_index_table_header_7') }}
                                    </th>
                                    <th>
                                        {{ __('clients.clients_subscription_index_table_header_8') }}
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
{{--                                @foreach ($subscriptions as $subscription)--}}
{{--                                    <tr>--}}
{{--                                        <td>--}}
{{--                                            <a href="{{ route('clients.subscriptions.edit', ['id' => $subscription['id']]) }}">--}}
{{--                                                {{$subscription['login']}}--}}
{{--                                            </a>--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            {{$subscription['pass']}}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            {{$subscription['expire']}}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            {{$subscription['login']}}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            {{$subscription['notifyexpiry']}}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            {{!empty($client['location']) ? $client['location'] : '-'}}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            <a class="btn btn-primary btn-modal" data-idabonement="{{ $subscription['id'] }}">{{ __('clients.clients_subscription_index_btn_2') }}</a>--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
{{--                                @endforeach--}}
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection
