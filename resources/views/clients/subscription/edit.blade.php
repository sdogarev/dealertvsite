@extends('layouts.app')
@section('title', __('subscription.page_title.app_header_edit_sub'))
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title" style="margin-bottom: 35px;">
                {{ __('subscription.page_title.app_header_edit_sub') }}
            </h3>
            <div class="page-bar" style="display: none">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Data Tables</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Editable Datatables</a>
                    </li>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                            Actions <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="#">Action</a>
                            </li>
                            <li>
                                <a href="#">Another action</a>
                            </li>
                            <li>
                                <a href="#">Something else here</a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="#">Separated link</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">

                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(!empty($_GET['status']) && $_GET['status'] == 'success')
                        <div class="note note-success">
                            {{ $_GET['message'] }}
                        </div>
                    @endif
                        <div class="portlet-body form">
                            <form class="form-horizontal" role="form" action="{{route('subscription.update', ['id' => $subscription['id']])}}" method="post">
                                @csrf
                                <input type="hidden" name="user_id" value="{{ $subscription['id'] }}">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_subscription_edit_label_1') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" style="width: 30%" name="password" class="form-control input-lg" value="{{ $subscription['pass'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_subscription_edit_label_2') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" style="width: 30%" name="protect_code" class="form-control input-lg" value="{{ $subscription['protect_code'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_subscription_edit_label_3') }}</label>
                                        <div class="col-md-9">
                                            <select name="stream_server" style="width: 30%" class="form-control input-lg">
                                                <option value="{{ $subscription['stream_server'] }}">{{ $subscription['stream_server'] }}</option>
                                                @foreach( $subscription['broadcast_servers'] as $broadcast_server)
                                                <option value="{{ $broadcast_server['name'] }}">{{ $broadcast_server['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_subscription_edit_label_4') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" name="timezone" style="width: 30%" class="form-control input-lg" value="{{ $subscription['timezone'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_subscription_edit_label_5') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" name="timeshift" style="width: 30%" class="form-control input-lg" value="{{ $subscription['timeshift'] }}">
                                        </div>
                                    </div>ln -s /etc/nginx/sites-available/dealerapi.conf
                                    {{--                                    <div class="form-group">--}}
{{--                                        <label class="col-md-3 control-label">{{ __('clients.clients_subscription_edit_label_6') }}</label>--}}
{{--                                        <div class="col-md-9">--}}
{{--                                            <input type="text" name="bitratehd" style="width: 30%" class="form-control input-lg" value="{{ $subscription['bitratehd'] }}">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="col-md-3 control-label">{{ __('clients.clients_subscription_edit_label_7') }}</label>--}}
{{--                                        <div class="col-md-9">--}}
{{--                                            <input type="text" name="archivebitratesd" style="width: 30%" class="form-control input-lg" value="{{ $subscription['archivebitratesd'] }}">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="col-md-3 control-label">{{ __('clients.clients_subscription_edit_label_8') }}</label>--}}
{{--                                        <div class="col-md-9">--}}
{{--                                            <input type="text" name="company" style="width: 30%" class="form-control input-lg" value="{{ $subscription['protect_code'] }}">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                </div>
                                <div class="form-actions right1">
                                    <a href="{{ url()->previous() }}" class="btn default">{{ __('clients.clients_btn_cancel') }}</a>
                                    <button type="submit" class="btn green">{{ __('clients.clients_btn_submit') }}</button>
                                </div>
                            </form>
                        </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection
