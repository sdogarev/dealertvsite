@extends('layouts.app')
@section('title', __('clients.page_title.app_header_edit_clients'))
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title" style="margin-bottom: 35px;">
                {{ __('clients.page_title.app_header_edit_clients') }}
            </h3>
            <div class="page-bar" style="display:none;">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Data Tables</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Editable Datatables</a>
                    </li>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                            Actions <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="#">Action</a>
                            </li>
                            <li>
                                <a href="#">Another action</a>
                            </li>
                            <li>
                                <a href="#">Something else here</a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="#">Separated link</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">

                    <!-- BEGIN SAMPLE FORM PORTLET-->
                        <div class="portlet-body form">
                            <form class="form-horizontal" method="POST" action="{{ route('client.update', ['id' => $client['id']]) }}" role="form">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_edit_label_1') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" name="name" style="width: 30%" class="form-control input-lg" value="{{$client['name']}}">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_edit_label_2') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" name="email" style="width: 30%" class="form-control input-lg" value="{{$client['email']}}">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_edit_label_5') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" name="phone" style="width: 30%" class="form-control input-lg" value="{{$client['phone']}}">
                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_edit_label_6') }}</label>
                                        <div class="col-md-9">
                                            <select name="person" style="width: 30%;" class="form-control input-lg">
                                                <option value="individual" {{$client['person'] == 'individual' ? 'selected' : ''}}>Физическое лицо</option>
                                                <option value="legal" {{$client['person'] == 'legal' ? 'selected' : ''}}>Юридическое лицо</option>
                                            </select>
                                            {{--                                            <input type="text" name="country" style="width: 30%" class="form-control input-lg" value="{{$client['country']}}">--}}
                                            @error('person')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_edit_label_7') }}</label>
                                        <div class="col-md-9">
                                            <select name="country" style="width: 30%;" class="form-control input-lg">
                                                <option value="{{$client['country']}}">{{ $client['country_name'] }}</option>
                                                @foreach($countries as $country)
                                                <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                                                @endforeach
                                            </select>
{{--                                            <input type="text" name="country" style="width: 30%" class="form-control input-lg" value="{{$client['country']}}">--}}
                                            @error('country')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_edit_label_8') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" name="city" style="width: 30%" class="form-control input-lg" value="{{$client['city']}}">
                                            @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_edit_label_9') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" name="address" style="width: 30%" class="form-control input-lg" value="{{$client['address']}}">
                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{{ __('clients.clients_edit_label_10') }}</label>
                                        <div class="col-md-9">
                                            <input type="text" name="zip" style="width: 30%" class="form-control input-lg" value="{{$client['zip']}}">
                                            @error('zip')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions right1">
                                    <a href="{{ url()->previous() }}" class="btn default">{{ __('clients.clients_btn_cancel') }}</a>
                                    <button type="submit" class="btn green">{{ __('clients.clients_btn_submit') }}</button>
                                </div>
                            </form>
                        </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>
            <!-- END PAGE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
@endsection
