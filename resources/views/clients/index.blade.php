@extends('layouts.app')
@section('title', __('clients.page_title.app_header_clients'))
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title" style="margin-bottom: 35px;">
            {{ __('clients.page_title.app_header_clients') }}
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb" style="display: none">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="index.html">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Data Tables</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Editable Datatables</a>
                </li>
            </ul>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box">
                    <div class="portlet-title">
                        @if(!empty($_GET['status']) && $_GET['status'] == 'success')
                            <div class="note note-success">
                                {{ $_GET['message'] }}
                            </div>
                        @endif
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{ route('client.create') }}" id="sample_editable_1_new" class="btn green">
                                            {{ __('clients.clients_index_btn_1') }} <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="btn-group pull-right">
                                        <button class="btn dropdown-toggle" style="display: none" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="javascript:;">
                                                    Print </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    Save as PDF </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    Export to Excel </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="url_clients_edit" value="{{ route('client.edit', ['id' => 'XXX']) }}">
                        <input type="hidden" id="url_clients_sub" value="{{ route('client.subscriptions.index', ['id' => 'XXX']) }}">
                        <input type="hidden" id="url_clients_sub_active" value="{{ route('client.subscriptions.active.index', ['id' => 'XXX']) }}">
                        <input type="hidden" id="url_clients_sub_expire" value="{{ route('client.subscriptions.expired.index', ['id' => 'XXX']) }}">
                        <input type="hidden" id="url_ajax" value="{{ route('clients.ajax.index') }}">
                        <table class="table table-striped table-hover table-bordered" id="table_clients_index">
                            <thead>
                            <tr>
                                <th>
                                    {{ __('clients.clients_index_table_header_1') }}
                                </th>
                                <th>
                                    {{ __('clients.clients_index_table_header_2') }}
                                </th>
                                <th>
                                    {{ __('clients.clients_index_table_header_3') }}
                                </th>
                                <th>
                                    {{ __('clients.clients_index_table_header_4') }}
                                </th>
                                <th>
                                    {{ __('clients.clients_index_table_header_5') }}
                                </th>
                                <th>
                                    {{ __('clients.clients_index_table_header_6') }}
                                </th>
                                <th>
                                    {{ __('clients.clients_index_table_header_7') }}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
{{--                            @foreach ($referals as $referal)--}}
{{--                                <tr>--}}
{{--                                <td>--}}
{{--                                    <a href="{{ route('clients.edit', ['id' => $referal['id']]) }}">--}}
{{--                                    {{$referal['name']}}--}}
{{--                                    </a>--}}
{{--                                </td>--}}
{{--                                <td>--}}
{{--                                    {{$referal['email']}}--}}
{{--                                </td>--}}
{{--                                <td>--}}
{{--                                    {{$referal['phone']}}--}}
{{--                                </td>--}}
{{--                                <td>--}}
{{--                                    <a href="{{ route('clients.subscriptions.index', ['id' => $referal['id']]) }}">{{$referal['subscription']}}</a>--}}
{{--                                </td>--}}
{{--                                <td>--}}
{{--                                    <a href="{{ route('clients.subscriptions.active.index', ['id' => $referal['id']]) }}">{{$referal['subscritpion_active']}}</a>--}}
{{--                                </td>--}}
{{--                                <td>--}}
{{--                                    <a href="{{ route('clients.subscriptions.expired.index', ['id' => $referal['id']]) }}">{{$referal['subscritpion_expend']}}</a>--}}
{{--                                </td>--}}
{{--                                <td>--}}
{{--                                    <a class="btn btn-primary btn-modal" href="{{ route('subscription.create', ['client_id' => $referal['id']]) }}" data-idabonement="{{ $referal['id'] }}">{{ __('clients.clients_index_btn_2') }}</a>--}}
{{--                                </td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
</div>
<!-- END CONTENT -->
<!-- END QUICK SIDEBAR -->
@endsection
