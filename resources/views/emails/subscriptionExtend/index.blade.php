@component('mail::message')

<p>Здравствуйте!</p>
<p>Благодарим Вас за продление абонемента русскоязычного телевидения Sunduk.tv.</p>

<p>Ваш логин: {{ $user->login }}</p>
<p>Ваш пароль:  {{ $user->pass }}</p>
<p>Ваш код родительского контроля: {{ $user->protect_code }}</p>
<p>Доступен до {{ $user->expire }}</p>

<p>Для просмотра русскоязычного телевидения Sunduk.TV выберите один из подходящих для вас способов, описанных на нашей странице:</p>
<p>
    <a href="https://www.sunduk.tv/watch" target="_blank" style="color: #5995ff !important; text-decoration: underline;">
    <span style="color: #5995ff;">
        https://www.sunduk.tv/watch
    </span>
    </a>
</p>
<p>
    Если у Вас возникнут вопросы незамедлительно обращайтесь в нашу службу поддержки через контактную форму:
    <a href="http://www.sunduk.tv/contacts" target="_blank" style="color: #5995ff !important; text-decoration: underline;">
    <span style="color: #5995ff;">
        http://www.sunduk.tv/contacts
    </span>
    </a>
</p>
<p>Приятного просмотра!</p>
<p>С уважением, Sunduk.TV</p>

{{--@component('mail::button', ['url' => ''])--}}
{{--Button Text--}}
{{--@endcomponent--}}
@endcomponent
