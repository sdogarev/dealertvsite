<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Языковые ресурсы абонементов
    |--------------------------------------------------------------------------
    |
    | Последующие языковые строки возвращают ошибки, текст, заголовки и т.д
    | связанные с Клиентами
    |
    |
    */

    'page_title' => [
        'app_header_clients' => 'Список клиентов',
        'app_header_edit_clients' => 'Редактирование клиента',
        'app_header_index_sub_clients' => 'Абонементы клиента',
        'app_header_create_clients' => 'Добавить нового клиента',
        'main_title' => 'Создать новый абонемент'
    ],

    /**
     * --------------------------------------------------------------------------
     * Clients Create
     * --------------------------------------------------------------------------
     */
    'main_title' => 'Добавить нового клиента',
    'clients_сreate_label_1' => 'Имя',
    'clients_сreate_label_2' => 'Email',
    'clients_сreate_label_3' => 'Пароль',
    'clients_сreate_label_4' => 'Повторить пароль',
    'clients_сreate_label_5' => 'Телефон',
    'clients_сreate_label_6' => 'Страна',
    'clients_сreate_label_7' => 'Город',
    'clients_сreate_label_8' => 'Улица и номер дома',
    'clients_сreate_label_9' => 'Почтовый индекс',
    'clients_сreate_label_10' => 'Название компании',
    'clients_сreate_label_11' => 'Номер НДС',
    'clients_btn_cancel' => 'Назад',
    'clients_btn_submit' => 'Отправить',

    /**
     * --------------------------------------------------------------------------
     * Clients Edit
     * --------------------------------------------------------------------------
     */
//    'main_title' => 'Добавить нового клиента',
    'clients_edit_label_1' => 'Имя',
    'clients_edit_label_2' => 'Email',
    'clients_edit_label_3' => 'Пароль',
    'clients_edit_label_4' => 'Повторить пароль',
    'clients_edit_label_5' => 'Телефон',
    'clients_edit_label_6' => 'Лицо',
    'clients_edit_label_7' => 'Страна',
    'clients_edit_label_8' => 'Город',
    'clients_edit_label_9' => 'Улица и номер дома',
    'clients_edit_label_10' => 'Почтовый индекс',

    /**
     * --------------------------------------------------------------------------
     * Clients Index
     * --------------------------------------------------------------------------
     */
//    'main_title' => 'Добавить нового клиента',
    'clients_index_btn_1' => 'Создать клиента',
    'clients_index_btn_2' => 'Создать',
    'clients_index_table_header_1' => 'Имя',
    'clients_index_table_header_2' => 'Майл',
    'clients_index_table_header_3' => 'Телефон',
    'clients_index_table_header_4' => 'Абонементы',
    'clients_index_table_header_5' => 'Активные абонементы',
    'clients_index_table_header_6' => 'Не Активные абонементы',
    'clients_index_table_header_7' => 'Создать абонемент',

    /**
     * --------------------------------------------------------------------------
     * Clients Subscription Index
     * --------------------------------------------------------------------------
     */
//    'main_title' => 'Добавить нового клиента',
    'clients_subscription_index_btn_1' => 'Создать абонемент',
    'clients_subscription_index_btn_2' => 'Продлить',
    'clients_subscription_index_table_header_1' => 'Логин',
    'clients_subscription_index_table_header_2' => 'Пароль',
    'clients_subscription_index_table_header_3' => 'Дата окончания',
    'clients_subscription_index_table_header_4' => 'Тариф',
    'clients_subscription_index_table_header_5' => 'Сервер вещания',
    'clients_subscription_index_table_header_6' => 'Часовая зона',
    'clients_subscription_index_table_header_7' => 'Статус',
    'clients_subscription_index_table_header_8' => 'Продлить',

    /**
     * --------------------------------------------------------------------------
     * Clients Subscription Edit
     * --------------------------------------------------------------------------
     */
//    'main_title' => 'Добавить нового клиента',
    'clients_edit_index_btn_1' => 'Создать абонемент',
    'clients_subscription_edit_label_1' => 'Пароль',
    'clients_subscription_edit_label_2' => 'Родительский пароль',
    'clients_subscription_edit_label_3' => 'Сервер вещания',
    'clients_subscription_edit_label_4' => 'Часовая зона',
    'clients_subscription_edit_label_5' => 'Cдвиг времени',
    'clients_subscription_edit_label_6' => 'Bitrate FullHD',
    'clients_subscription_edit_label_7' => 'Bitrate SD',
    'clients_subscription_edit_label_8' => 'Bitrate Видеотека',
];
