<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Языковые ресурсы абонементов
    |--------------------------------------------------------------------------
    |
    | Последующие языковые строки возвращают ошибки, текст, заголовки и т.д
    | связанные с Тестовыми абонементами
    |
    |
    */

    'page_title' => [
        'app_header_index_demo' => 'Тестовые абонементы',
        'app_header_create_demo' => 'Создать тестовый абонемент',
        'app_header_edit_demo' => 'Редактирование тестовых абонементов',
        'main_title' => 'Создать новый абонемент'
    ],

    /**
     * --------------------------------------------------------------------------
     * Demo Index
     * --------------------------------------------------------------------------
     */
    'demo_index_message' => 'Внимание! Лимит создания тестовых абонементов составляет - :maxtestuser шт. в месяц!',
    'demo_index_btn' => 'Создать тестовый абонемент',
    'demo_index_table_header_1' => 'Логин',
    'demo_index_table_header_2' => 'Пароль',
    'demo_index_table_header_3' => 'Код протекции',
    'demo_index_table_header_4' => 'Дата создания',
    'demo_index_table_header_5' => 'Дата окончания',
    'demo_index_table_header_6' => 'Статус',
    'demo_index_status_1' => 'Активен',
    'demo_index_status_2' => 'Завершён',
    'demo_btn_cancel' => 'Назад',
    'demo_btn_submit' => 'Создать',
    /**
     * --------------------------------------------------------------------------
     * Demo Create
     * --------------------------------------------------------------------------
     */
    'demo_create_label_1' => 'Сервер вещания',
    'demo_create_label_2' => 'Часовая зона',
    'demo_create_label_3' => 'Временной сдвиг',
    'demo_create_label_4' => 'Bitrate HD',
    'demo_create_label_5' => 'Bitrate SD',
    'demo_create_label_6' => 'Bitrate Видеотека',


    /**
     * --------------------------------------------------------------------------
     * Demo  Edit
     * --------------------------------------------------------------------------
     */
//    'main_title' => 'Добавить нового клиента',
    'clients_edit_index_btn_1' => 'Создать абонемент',
    'demo_edit_label_1' => 'Пароль',
    'demo_edit_label_2' => 'Родительский пароль',
    'demo_edit_label_3' => 'Сервер вещания',
//    'clients_subscription_edit_label_2' => 'Родительский пароль',
//    'clients_subscription_edit_label_3' => 'Сервер вещания',
//    'clients_subscription_edit_label_4' => 'Часовая зона',
//    'clients_subscription_edit_label_5' => 'Cдвиг времени',
//    'clients_subscription_edit_label_6' => 'Bitrate FullHD',
//    'clients_subscription_edit_label_7' => 'Bitrate SD',
//    'clients_subscription_edit_label_8' => 'Bitrate Видеотека',
];
